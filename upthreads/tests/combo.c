/**************************************************************************
 * combo.c
 * Tests a combination of featurs
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include <string.h>

#include "pthread.h"
#include "mqueue.h"
#include "mqueue-fcntl.h"
#include "semaphore.h"

#include "util.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

#define TEST_MESSAGE "This is a test and only a test"
#define TEST_MSGLEN  (strlen(TEST_MESSAGE)+1)

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

static pthread_t poster;
static sem_t sem;
static pthread_mutex_t mutex;
static pthread_cond_t cond;
static volatile int data_available = 0;

/**************************************************************************
 * Private Functions
 **************************************************************************/

static void show_sem_value(const char *name, int count, int *err)
{
  int status;
  int value;

  status = sem_getvalue(&sem, &value);
  if (status < 0)
    {
      util_printf("%s: ERROR count=%d Could not get semaphore value\n", name, count);
      (*err)++;
    }
  else
    {
      util_printf("%s: count=%d semaphore value=%d\n", name, count, value);
    }
}

static void take_semaphore(const char *name, int count, int *err)
{
  int status;

  util_printf("%s: count=%d Waiting on semaphore\n", name, count);
  status = sem_wait(&sem);
  if (status != 0)
    {
      util_printf("%s: ERROR count=%d sem_wait failed\n", name, count);
      (*err)++;
    }
  util_printf("%s: count=%d Awakened\n", name, count);
}

static void give_semaphore(const char *name, int count, int *err)
{
  int status;

  util_printf("%s: loop=%d Posting semaphore\n", name, count);
  status = sem_post(&sem);
  if (status != 0)
    {
      util_printf("%s: ERROR count=%d sem_wait failed\n", name, count);
      if (err) (*err)++;
    }

  pthread_yield();
}

static void lock_mutex(const char *name, int count, int *err)
{
  int status;

  util_printf("%s: count=%d Locking mutex\n", name, count);
  status = pthread_mutex_lock(&mutex);
  if (status != 0)
    {
      util_printf("%s: ERROR count= %d pthread_mutex_lock failed, status=%d\n", name, count, status);
      (*err)++;
    }
  util_printf("%s: count=%d mutex locked\n", name, count);
}

static void unlock_mutex(const char *name, int count, int *err)
{
  int status;

  util_printf("%s: count=%d Locking mutex\n", name, count);
  status = pthread_mutex_unlock(&mutex);
  if (status != 0)
    {
      util_printf("%s: ERROR count=%d: pthread_mutex_unlock failed, status=%d\n",
                  name, count, status);
      (*err)++;
    }

  pthread_yield();
}

static void start_thread(const char *name, pthread_t *thread, void *entry(void *arg), int priority)
{
  pthread_attr_t attr;
  int status;

  status = pthread_attr_init(&attr);
  if (status != 0)
    {
      util_printf("%s: pthread_attr_init failed, status=%d\n", name, status);
    }

#ifdef CONFIG_MEMORY_ALLOCATOR
  status = pthread_attr_setstacksize(&attr, 16384);
  if (status != 0)
    {
      util_printf("%s: pthread_attr_setstacksize failed, status=%d\n", name, status);
    }
#endif

  attr.sched_priority = priority; /* Cheating because I don't want to include sched.h */

  status = pthread_create(thread, &attr, entry, NULL);
  if (status != 0)
    {
      util_printf("%s: pthread_create failed, status=%d\n", name, status);
    }
  pthread_yield();
}

static void join_thread(const char *name, pthread_t *thread, void *expected_value)
{
  void *result;

  pthread_join(*thread, &result);
  if (result != expected_value)
    {
      util_printf("%s: ERROR thread exited with %d(0x%08x); expected %d(0x%08x)\n",
                   name, (int)result, (int)result, (int)expected_value, (int)expected_value);
    }
  else
    {
      util_printf("%s: PASS thread exited as expected\n", name);
    }
}

static void *sender_thread(void *arg)
{
  mqd_t mqfd;
  char msg_buffer[TEST_MSGLEN];
  struct mq_attr attr;
  int status = 0;
  int nerrors = 0;
  int i;

  util_printf("sender: Starting\n");

  /* Fill in attributes for message queue */

  attr.mq_maxmsg  = 20;
  attr.mq_msgsize = TEST_MSGLEN;
  attr.mq_flags   = 0;

  /* Set the flags for the open of the queue.
   * Make it a blocking open on the queue, meaning it will block if
   * this process tries to send to the queue and the queue is full.
   *
   *   O_CREAT - the queue will get created if it does not already exist.
   *   O_WRONLY - we are only planning to write to the queue.
   *
   * Open the queue, and create it if the receiving process hasn't
   * already created it.
   */

  mqfd = mq_open("testmq", O_WRONLY|O_CREAT, 0666, &attr);
  if (mqfd < 0)
    {
	util_printf("sender: ERROR mq_open failed\n");
        pthread_exit((void*)1);
    }

  /* Fill in a test message buffer to send */

  memcpy(msg_buffer, TEST_MESSAGE, TEST_MSGLEN);

  /* Perform the send 10 times */

  for (i = 0; i < 10; i++)
    {
      /* Before each send, take the semaphore */

      show_sem_value("sender", i, &nerrors);
      take_semaphore("sender", i, &nerrors);
      show_sem_value("sender", i, &nerrors);

      /* Send the message */

      status = mq_send(mqfd, msg_buffer, TEST_MSGLEN, 42);
      if (status < 0)
        {
          util_printf("sender: ERROR count=%d mq_send failed\n", i);
          nerrors++;
        }
      else
        {
          util_printf("sender: count=%d mq_send succeeded\n", i);
        }

      show_sem_value("sender", i, &nerrors);
      give_semaphore("sender", i, &nerrors);
      show_sem_value("sender", i, &nerrors);
    }

  util_printf("sender: Canceling poster thread\n");
  status = pthread_cancel(poster);
  if (status != 0)
    {
      util_printf("sender: ERROR pthread_cancel failed, status=%d\n", status);
    }

  /* Close the queue and return success */

  if (mq_close(mqfd) < 0)
    {
      util_printf("sender: ERROR mq_close failed\n");
    }

  util_printf("sender: returning nerrors=%d\n", nerrors);
  return (void*)nerrors;
}

static void *signaler_thread(void *arg)
{
  mqd_t mqfd;
  char msg_buffer[TEST_MSGLEN];
  struct mq_attr attr;
  int nbytes;
  int nerrors = 0;
  int status;
  int i;

  util_printf("signaler: Starting\n");

  /* Fill in attributes for message queue */

  attr.mq_maxmsg  = 20;
  attr.mq_msgsize = TEST_MSGLEN;
  attr.mq_flags   = 0;

  /* Set the flags for the open of the queue.
   * Make it a blocking open on the queue, meaning it will block if
   * this process tries to* send to the queue and the queue is full.
   *
   *   O_CREAT - the queue will get created if it does not already exist.
   *   O_RDONLY - we are only planning to write to the queue.
   *
   * Open the queue, and create it if the sending process hasn't
   * already created it.
   */

   mqfd = mq_open("testmq", O_RDONLY|O_CREAT, 0666, &attr);
   if (mqfd < 0)
     {
       util_printf("signaler: ERROR mq_open failed\n");
       pthread_exit((void*)1);
     }

   /* Perform the receive 10 times */

   for (i = 0; i < 10; i++)
    {
      util_printf("signaler: count=%d receiving message\n", i);

      nbytes = mq_receive(mqfd, msg_buffer, TEST_MSGLEN, 0);
      if (nbytes < 0)
        {
          util_printf("signaler: ERROR count=%d q_receive failure on msg %d\n", i);
          nerrors++;
        }
      else if (nbytes != TEST_MSGLEN)
        {
          util_printf("signaler: ERROR ount=%d mq_receive return bad size %d on msg %d\n", nbytes, i);
          nerrors++;
        }
      else if (memcmp(TEST_MESSAGE, msg_buffer, nbytes) != 0)
        {
	  int j;
          util_printf("signaler: ERROR count=%d mq_receive returned corrupt message on msg %d\n", nbytes, i);
	  util_printf("          i  Expected Received\n");
          for (j = 0; j < TEST_MSGLEN-1; j++)
	  {
            util_printf("          %2d %02x (%c) %02x\n",
                        j, TEST_MESSAGE[j], TEST_MESSAGE[j], msg_buffer[j] & 0x0ff);
	  }
          util_printf("          %2d 00     %02x\n", msg_buffer[j] & 0xff);
          nerrors++;
        }
      else
        {
          util_printf("signaler: count=%d mq_receive succeeded\n", nbytes, i);
        }

      if (data_available)
        {
          util_printf("signaler: ERROR count=%d data already available\n", i);
          nerrors++;
        }

      /* Set data available and signal the waiter */

      lock_mutex("signaler", i, &nerrors);
      util_printf("signaler: count=%d signaling condition\n", i);

      data_available = 1;
      status = pthread_cond_signal(&cond);
      if (status != 0)
        {
          util_printf("signaler: ERROR count=%d pthread_cond_signal failed, status=%d\n", i, status);
          nerrors++;
        }

      /* Take the semaphore */

      show_sem_value("sender", i, &nerrors);
      take_semaphore("sender", i, &nerrors);
      show_sem_value("sender", i, &nerrors);

      unlock_mutex("sender", i, &nerrors);
    }

  /* Close the queue and return success */

  if (mq_close(mqfd) < 0)
    {
      util_printf("signaler: ERROR mq_close failed\n");
      nerrors++;
    }

  /* Destroy the queue */

  if (mq_unlink("testmq") < 0)
    {
      util_printf("signaler: ERROR mq_close failed\n");
      nerrors++;
    }

  util_printf("signaler: returning nerrors=%d\n", nerrors);
  pthread_exit((void*)nerrors);
  return (void*)nerrors;
}

static void *poster_thread(void *arg)
{
  int nerrors = 0;
  int status;
  int count;

  util_printf("poster: Starting\n");
  for(count = 0; 1; count++)
    {
      lock_mutex("poster", count, &nerrors);

      if (!data_available)
        {
           /* We are higher priority than the signaler thread so the
            * only time that the signaler thread will have a chance to run is when
            * we are waiting for the condition variable.  In this case, pthread_cond_wait
            * will automatically release the mutex for the signaler (then re-acquire
            * the mutex before returning.
            */

           util_printf("poster: count=%d waiting for condition\n", count);
           status = pthread_cond_wait(&cond, &mutex);
           if (status != 0)
             {
               util_printf("poster: ERROR count=%d pthread_cond_wait failed, status=%d\n", count, status);
               nerrors++;
             }
           util_printf("poster: count=%d Awakened\n", count);
        }

      /* No data should be available */

      if (!data_available)
        {
          util_printf("poster: ERROR count=%d data not available after wait\n");
          nerrors++;
        }

      /* Clear data available */

      data_available = 0;

      show_sem_value("poster", count, &nerrors);
      give_semaphore("poster", count, &nerrors);
      show_sem_value("poster", count, &nerrors);

      unlock_mutex("poster", count, &nerrors);
    }
}

/**************************************************************************
 * Public Functions
 **************************************************************************/

int main(int argc, char **argv, char **envp)
{
  pthread_t sender;
  pthread_t signaler;

  util_printf("main: Initializing semaphore to 0\n");
  sem_init(&sem, 0, 1);

  util_printf("Initializing mutex\n");
  pthread_mutex_init(&mutex, NULL);

  /* Start the threads */

  util_printf("main: Starting signaler at priority 4\n");
  start_thread("main", &signaler, signaler_thread, 4);

  util_printf("main: Starting poster at priority 8\n");
  start_thread("main", &poster, poster_thread, 8);

  util_printf("main: Starting sender at priority 2\n");
  start_thread("main", &sender, sender_thread, 2);

  /* Join to each thread */

  util_printf("main: Joining to signaler thread\n");
  join_thread("main", &signaler, 0);

  util_printf("main: Joining to poster thread\n");
  join_thread("main", &poster, PTHREAD_CANCELED);

  util_printf("main: Joining to sender thread\n");
  join_thread("main", &sender, 0);

  sem_destroy(&sem);
  pthread_mutex_destroy(&mutex);
  return 0;
}


