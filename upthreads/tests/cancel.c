/***********************************************************************
 * upthreads/tests/cancel.c
 *
 *   Copyright (c) 2004, 2006, Gregory Nutt.  All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#include <time.h>
#include "pthread.h"
#include "pthread-errno.h"
#include "util.h"

#ifndef NULL
# define NULL (void*)0
#endif

static pthread_mutex_t mutex;
static pthread_cond_t  cond;

static void *thread_waiter(void *parameter)
{
  int status;

  /* Take the mutex */

  util_printf("waiter: Taking mutex\n");
  status = pthread_mutex_lock(&mutex);
  if (status != 0)
    {
       util_printf("waiter: ERROR pthread_mutex_lock failed, status=%d\n", status);
    }

  util_printf("waiter: Starting wait for condition\n");

  /* Are we a non-cancelable thread? */

  if (!parameter)
    {
      util_printf("waiter: Setting non-cancelable\n");
      status = pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
      if (status != 0)
        {
           util_printf("waiter: ERROR pthread_setcancelstate failed, status=%d\n", status);
        }
    }

  /* The wait -- we will never awaken from this. */

  status = pthread_cond_wait(&cond, &mutex);
  if (status != 0)
    {
      util_printf("waiter: ERROR pthread_cond_wait failed, status=%d\n", status);
    }

  /* Release the mutex */

  util_printf("waiter: Releasing mutex\n");
  status = pthread_mutex_unlock(&mutex);
  if (status != 0)
    {
      util_printf("ERROR waiter: pthread_mutex_unlock failed, status=%d\n", status);
    }

  util_printf("waiter: Exit with status 0x12345678\n");
  pthread_exit((void*)0x12345678);
  return NULL;
}

static void start_thread(pthread_t *waiter, int cancelable)
{
  pthread_attr_t attr;
  int status;

  /* Initialize the mutex */

  util_printf("start_thread: Initializing mutex\n");
  status = pthread_mutex_init(&mutex, NULL);
  if (status != 0)
    {
      util_printf("start_thread: ERROR pthread_mutex_init failed, status=%d\n", status);
    }

  /* Initialize the condition variable */

  util_printf("start_thread: Initializing cond\n");
  status = pthread_cond_init(&cond, NULL);
  if (status != 0)
    {
      util_printf("start_thread: ERROR pthread_cond_init failed, status=%d\n", status);
    }

  /* Set up attributes */

  status = pthread_attr_init(&attr);
  if (status != 0)
    {
      util_printf("main: pthread_attr_init failed, status=%d\n", status);
    }

#ifdef CONFIG_MEMORY_ALLOCATOR
  status = pthread_attr_setstacksize(&attr, 16384);
  if (status != 0)
    {
      util_printf("main: pthread_attr_setstacksize failed, status=%d\n", status);
    }
#endif

  /* Start the waiter thread  */

  util_printf("start_thread: Starting thread\n");
  status = pthread_create(waiter, NULL, thread_waiter, (void*)cancelable);
  if (status != 0)
    {
      util_printf("start_thread: ERROR pthread_create failed, status=%d\n", status);
    }

  /* Make sure that the waiter thread gets a chance to run */

  util_printf("start_thread: Yielding\n");
  pthread_yield();

}

static void restart_thread(pthread_t *waiter, int cancelable)
{
  int status;

  /* Destroy the condition variable */

  util_printf("restart_thread: Destroying cond\n");
  status = pthread_cond_destroy(&cond);
  if (status != 0)
    {
      util_printf("restart_thread: ERROR pthread_cond_destroy failed, status=%d\n", status);
    }

  /* Destroy the mutex */

  util_printf("restart_thread: Destroying mutex\n");
  status = pthread_mutex_destroy(&mutex);
  if (status != 0)
    {
      util_printf("restart_thread: ERROR pthread_mutex_destroy failed, status=%d\n", status);
    }

  /* Then restart the thread */

  util_printf("restart_thread: Re-starting thread\n");
  start_thread(waiter, cancelable);
}

int main(int argc, char **argv, char **envp)
{
  pthread_t waiter;
  void *result;
  int status;

  /* Test 1: Normal Cancel *********************************************/
  /* Start the waiter thread  */

  util_printf("\nmain: Test 1: Normal Cancelation\n");
  util_printf("main: Starting thread\n");
  start_thread(&waiter, 1);

  /* Then cancel it.  It should be in the pthread_cond_wait now */

  util_printf("main: Canceling thread\n");
  status = pthread_cancel(waiter);
  if (status != 0)
    {
      util_printf("main: ERROR pthread_cancel failed, status=%d\n", status);
    }

  /* Then join to the thread to pick up the result */

  util_printf("main: Joining\n");
  status = pthread_join(waiter, &result);
  if (status != 0)
    {
      util_printf("main: ERROR pthread_join failed, status=%d\n", status);
    }
  else
    {
      util_printf("main: waiter exited with result=%p\n", result);
      if (result != PTHREAD_CANCELED)
        {
          util_printf("main: ERROR expected result=%p\n", PTHREAD_CANCELED);
        }
      else
        {
          util_printf("main: PASS thread terminated with PTHREAD_CANCELED\n");
        }
    }

  /* Test 2: Cancel Detached Thread ************************************/

  util_printf("\nmain: Test 2: Cancelation of detached thread\n");
  util_printf("main: Re-starting thread\n");
  restart_thread(&waiter, 1);

  /* Detach the thread */

  status = pthread_detach(waiter);
  if (status != 0)
    {
      util_printf("main: ERROR pthread_detach, status=%d\n", status);
    }

  /* Then cancel it.  It should be in the pthread_cond_wait now */

  util_printf("main: Canceling thread\n");
  status = pthread_cancel(waiter);
  if (status != 0)
    {
      util_printf("main: ERROR pthread_cancel failed, status=%d\n", status);
    }

  /* Join should now fail */

  util_printf("main: Joining\n");
  status = pthread_join(waiter, &result);
  if (status == 0)
    {
      util_printf("main: ERROR pthread_join succeeded\n");
    }
  else if (status != ESRCH)
    {
      util_printf("main: ERROR pthread_join failed but with wrong status=%d\n", status);
    }
  else
    {
      util_printf("main: PASS pthread_join failed with status=ESRCH\n");
    }

  /* Test 3: Non-cancelable threads ************************************/

  util_printf("\nmain: Test 3: Non-cancelable threads\n");
  util_printf("main: Re-starting thread (non-cancelable)\n");
  restart_thread(&waiter, 0);

  /* Then cancel it.  It should be in the pthread_cond_wait now */

  util_printf("main: Canceling thread\n");
  status = pthread_cancel(waiter);
  if (status == 0)
    {
      util_printf("main: ERROR pthread_cancel succeeded\n");
    }
  else if (status != ESRCH)
    {
      util_printf("main: ERROR pthread_cancel failed but with wrong status=%d\n", status);
    }
  else
    {
      util_printf("main: PASS pthread_cancel failed with status=ESRCH\n");
    }

 return 0;
}
