/***********************************************************************
 * upthreads/tests/mutex.c
 *
 *   Copyright (c) 2004, 2006, Gregory Nutt.  All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#include "pthread.h"
#include "util.h"

#ifndef NULL
# define NULL (void*)0
#endif

static pthread_mutex_t mut;
static volatile int my_mutex = 0;
static unsigned long nloops[2] = {0, 0};
static unsigned long nerrors[2] = {0, 0};

static void *thread_func(void *parameter)
{
  int my_id = (int)parameter;
  int my_ndx = my_id - 1;
  volatile int i;

  for (;;)
    {
      int status = pthread_mutex_lock(&mut);
      if (status != 0)
        {
          util_printf("ERROR thread %d: pthread_mutex_lock failed, status=%d\n",
                      my_id, status);
        }

      if (my_mutex == 1)
        {
          util_printf("ERROR thread=%d: "
                      "my_mutex should be zero, instead my_mutex=%d\n",
                       my_id, my_mutex);
          nerrors[my_ndx]++;
        }

      my_mutex = 1;	
      for (i = 0; i < 10; i++)
        {
          pthread_yield();
        }
      my_mutex = 0;

      status = pthread_mutex_unlock(&mut);
      if (status != 0)
        {
          util_printf("ERROR thread %d: pthread_mutex_unlock failed, status=%d\n",
                       my_id, status);
        }

      nloops[my_ndx]++;
    }	
}

static void signal_handler(int signo)
{
  util_printf("\tThread1\tThread2\n");
  util_printf("Loops\t%ld\t%ld\n", nloops[0], nloops[1]);
  util_printf("Errors\t%ld\t%ld\n", nerrors[0], nerrors[1]);
  util_exit(0);
}

int main(int argc, char **argv, char **envp)
{
  pthread_t thread1, thread2;

  /* Set up to catch control-C interrupt */

  (void)util_catchinterrupt(signal_handler);

  /* Initialize the mutex */

  util_printf("Initializing mutex\n");
  pthread_mutex_init(&mut, NULL);

  /* Start two thread instances */

  util_printf("Starting thread 1\n");
  if ((pthread_create(&thread1, NULL, thread_func, (void*)1)) != 0)
    {
      util_printf("Error in thread#1 creation\n");
    }

  util_printf("Starting thread 2\n");
  if ((pthread_create(&thread2, NULL, thread_func, (void*)2)) != 0)
    {
      util_printf("Error in thread#2 creation\n");
    }

  util_printf("Press control-C to terminate the example\n");

  pthread_join(thread1, NULL);
  pthread_join(thread2, NULL);
  pthread_mutex_destroy(&mut);
  return 0;
}
