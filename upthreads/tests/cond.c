/***********************************************************************
 * Upthreads/tests/mutex.c
 *
 *   Copyright (c) 2004, 2006, Gregory Nutt.  All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#include "pthread.h"
#include "util.h"

#ifndef NULL
# define NULL (void*)0
#endif

static volatile enum { RUNNING, MUTEX_WAIT, COND_WAIT} waiter_state;

static pthread_mutex_t mutex;
static pthread_cond_t  cond;
static volatile int data_available = 0;
static int waiter_nloops = 0;
static int waiter_waits = 0;
static int waiter_nerrors = 0;
static int signaler_nloops = 0;
static int signaler_already = 0;
static int signaler_state = 0;
static int signaler_nerrors = 0;

static void *thread_waiter(void *parameter)
{
  int status;

  for (;;)
    {
      /* Take the mutex */

      waiter_state = MUTEX_WAIT;
      status = pthread_mutex_lock(&mutex);
      waiter_state = RUNNING;

      if (status != 0)
        {
          util_printf("waiter: ERROR pthread_mutex_lock failed, status=%d\n", status);
          waiter_nerrors++;
        }

      /* Check if data is available -- if data is not available then
       * wait for it
       */

      if (!data_available)
        {
           /* We are higher priority than the signaler thread so the
            * only time that the signaler thread will have a chance to run is when
            * we are waiting for the condition variable.  In this case, pthread_cond_wait
            * will automatically release the mutex for the signaler (then re-acquire
            * the mutex before returning.
            */

           waiter_state = COND_WAIT;
           status = pthread_cond_wait(&cond, &mutex);
           waiter_state = RUNNING;

           if (status != 0)
             {
               util_printf("waiter: ERROR pthread_cond_wait failed, status=%d\n", status);
               waiter_nerrors++;
             }
           waiter_waits++;
        }

      /* No data should be available */

      if (!data_available)
        {
          util_printf("waiter: ERROR data not available after wait\n");
          waiter_nerrors++;
        }

      /* Clear data available */

      data_available = 0;

      /* Release the mutex */

      status = pthread_mutex_unlock(&mutex);
      if (status != 0)
        {
          util_printf("ERROR waiter: pthread_mutex_unlock failed, status=%d\n", status);
          waiter_nerrors++;
        }

      waiter_nloops++;
    }
}

static void *thread_signaler(void *parameter)
{
  int status;

  for (;;)
    {
      /* Take the mutex.  The waiter is higher priority an should
      * run until it waits for the condition.  So, at this point
      * signaler should be waiting for the condition.
      */

      status = pthread_mutex_lock(&mutex);
      if (status != 0)
        {
          util_printf("signaler: ERROR pthread_mutex_lock failed, status=%d\n", status);
          signaler_nerrors++;
        }

      /* Verify the state */

      if (waiter_state != COND_WAIT)
        {
          util_printf("signaler: ERROR waiter state = %d != COND_WAITING\n", waiter_state);
          signaler_state++;
        }

      if (data_available)
        {
          util_printf("signaler: ERROR data already available\n");
          signaler_already++;
        }

      /* Set data available and signal the waiter */

     data_available = 1;
     status = pthread_cond_signal(&cond);
      if (status != 0)
        {
          util_printf("signaler: ERROR pthread_cond_signal failed, status=%d\n", status);
          signaler_nerrors++;
        }

     /* Release the mutex */

      status = pthread_mutex_unlock(&mutex);
      if (status != 0)
        {
          util_printf("ERROR signaler: pthread_mutex_unlock failed, status=%d\n", status);
          signaler_nerrors++;
        }

      signaler_nloops++;
    }
}
static void signal_handler(int signo)
{
  util_printf("\tWaiter\tSignaler\n");
  util_printf("Loops\t%d\t%d\n", waiter_nloops, signaler_nloops);
  util_printf("Errors\t%d\t%d\n", waiter_nerrors, signaler_nerrors);
  util_printf("\n%d times, waiter did not have to wait for data\n", waiter_nloops - waiter_waits);
  util_printf("%d times, data was already available when the signaler run\n", signaler_already);
  util_printf("%d times, the waiter was in an unexpected state when the signaler ran\n", signaler_state);
  util_exit(0);
}

int main(int argc, char **argv, char **envp)
{
  pthread_t waiter;
  pthread_t signaler;
  pthread_attr_t attr;
  int status;

  /* Set up to catch control-C interrupt */

  (void)util_catchinterrupt(signal_handler);

  /* Initialize the mutex */

  util_printf("main: Initializing mutex\n");
  status = pthread_mutex_init(&mutex, NULL);
  if (status != 0)
    {
      util_printf("main: ERROR pthread_mutex_init failed, status=%d\n", status);
    }

  /* Initialize the condition variable */

  util_printf("main: Initializing cond\n");
  status = pthread_cond_init(&cond, NULL);
  if (status != 0)
    {
      util_printf("main: ERROR pthread_condinit failed, status=%d\n", status);
    }

  /* Start the waiter thread at higher priority */

  util_printf("main: Starting waiter\n");
  status = pthread_attr_init(&attr);
  if (status != 0)
    {
      util_printf("main: pthread_attr_init failed, status=%d\n", status);
    }

  attr.sched_priority = 8; /* Cheating because I don't want to include sched.h */

  status = pthread_create(&waiter, &attr, thread_waiter, NULL);
  if (status != 0)
    {
      util_printf("main: pthread_create failed, status=%d\n", status);
    }

  util_printf("main: Starting signaler\n");
  status = pthread_attr_init(&attr);
  if (status != 0)
    {
      util_printf("main: pthread_attr_init failed, status=%d\n", status);
    }

  attr.sched_priority = 4; /* Cheating because I don't want to include sched.h */

  status = pthread_create(&signaler, &attr, thread_signaler, NULL);
  if (status != 0)
    {
      util_printf("main: pthread_create failed, status=%d\n", status);
    }

  util_printf("main: Press control-C to terminate the example\n");

  pthread_join(waiter, NULL);
  pthread_join(signaler, NULL);
  return 0;
}
