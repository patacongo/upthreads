/***********************************************************************
 * upthreads/tests/timedwait.c
 *
 *   Copyright (c) 2004, 2006, Gregory Nutt.  All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#include <time.h>
#include "pthread.h"
#include "util.h"

#ifndef NULL
# define NULL (void*)0
#endif

#ifdef CONFIG_PTHREAD_TIMER
static pthread_mutex_t mutex;
static pthread_cond_t  cond;

static void *thread_waiter(void *parameter)
{
  struct timespec time;
  int status;

  /* Take the mutex */

  util_printf("waiter: Taking mutex\n");
  status = pthread_mutex_lock(&mutex);
  if (status != 0)
    {
         util_printf("waiter: ERROR pthread_mutex_lock failed, status=%d\n", status);
    }

  util_printf("waiter: Starting 5 second wait for condition\n");

  pthread_time(&time);
  time.tv_sec += 5;

  /* The wait -- no-one is ever going to awaken us */

  status = pthread_cond_timedwait(&cond, &mutex, &time);
  if (status != 0)
    {
      util_printf("waiter: ERROR pthread_cond_timedwait failed, status=%d\n", status);
    }

  /* Release the mutex */

  util_printf("waiter: Releasing mutex\n");
  status = pthread_mutex_unlock(&mutex);
  if (status != 0)
    {
      util_printf("ERROR waiter: pthread_mutex_unlock failed, status=%d\n", status);
    }

  util_printf("waiter: Exit with status 0x12345678\n");
  pthread_exit((void*)0x12345678);
  return NULL;
}
#else
# warning "Timers are not enabled in this configuration"
#endif

int main(int argc, char **argv, char **envp)
{
#ifdef CONFIG_PTHREAD_TIMER
  pthread_t waiter;
  pthread_attr_t attr;
  void *result;
  int status;

  /* Initialize the mutex */

  util_printf("main: Initializing mutex\n");
  status = pthread_mutex_init(&mutex, NULL);
  if (status != 0)
    {
      util_printf("main: ERROR pthread_mutex_init failed, status=%d\n", status);
    }

  /* Initialize the condition variable */

  util_printf("main: Initializing cond\n");
  status = pthread_cond_init(&cond, NULL);
  if (status != 0)
    {
      util_printf("main: ERROR pthread_condinit failed, status=%d\n", status);
    }

  /* Start the waiter thread at higher priority */

  util_printf("main: Starting waiter\n");
  status = pthread_attr_init(&attr);
  if (status != 0)
    {
      util_printf("main: pthread_attr_init failed, status=%d\n", status);
    }

  attr.sched_priority = 8; /* Cheating because I don't want to include sched.h */

  status = pthread_create(&waiter, &attr, thread_waiter, NULL);
  if (status != 0)
    {
      util_printf("main: pthread_create failed, status=%d\n", status);
    }

  util_printf("main: Joining");
  status = pthread_join(waiter, &result);
  if (status != 0)
    {
      util_printf("main: ERROR pthread_join failed, status=%d\n", status);
    }
  else
    {
      util_printf("main: waiter exited with result=%p\n", result);
    }
#else
  util_printf("Timers are not enabled in this configuration\n");
#endif
  return 0;
}
