/***********************************************************************
 * upthreads/tests/sem.c
 *
 *   Copyright (c) 2004, 2006, Gregory Nutt.  All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#include "pthread.h"
#include "semaphore.h"
#include "util.h"

#ifndef NULL
# define NULL (void*)0
#endif

static sem_t sem;

static void *waiter_func(void *parameter)
{
  int my_id = (int)parameter;
  int status;
  int value;

  util_printf("Thread %d: Started\n", my_id);

  /* Take the semaphore */

  status = sem_getvalue(&sem, &value);
  if (status < 0)
    {
      util_printf("ERROR thread %d: Could not get semaphore value\n", my_id);
    }
  else
    {
      util_printf("Thread %d: Initial semaphore value = %d\n", my_id, value);
    }

  util_printf("Waiting on semaphore\n", my_id);
  status = sem_wait(&sem);
  if (status != 0)
    {
      util_printf("ERROR thread %d: sem_wait failed\n", my_id);
    }
  util_printf("Thread %d: Awakened\n", my_id);

  status = sem_getvalue(&sem, &value);
  if (status < 0)
    {
      util_printf("ERROR thread %d: Could not get semaphore value\n", my_id);
    }
  else
    {
      util_printf("Thread %d: New semaphore value = %d\n", my_id, value);
    }

  util_printf("Thread %d: Done\n", my_id);
  return NULL;
}

static void *poster_func(void *parameter)
{
  int my_id = (int)parameter;
  int status;
  int value;

  util_printf("Thread %d: Started\n", my_id);

  /* Take the semaphore */

  do
    {
      status = sem_getvalue(&sem, &value);
      if (status < 0)
        {
          util_printf("ERROR thread %d: Could not get semaphore value\n", my_id);
        }
      else
        {
          util_printf("Thread %d: semaphore value = %d\n", my_id, value);
        }

      if (value < 0)
        {
          util_printf("Thread %d: Posting semaphore\n", my_id);
          status = sem_post(&sem);
          if (status != 0)
            {
              util_printf("ERROR thread %d: sem_wait failed\n", my_id);
            }

          pthread_yield();

          status = sem_getvalue(&sem, &value);
          if (status < 0)
            {
              util_printf("ERROR thread %d: Could not get semaphore value\n" , my_id);
            }
          else
            {
              util_printf("Thread %d: New semaphore value = %d\n", my_id, value);
            }
        }
    }
  while (value < 0);

  util_printf("Thread %d: Done\n", my_id);
  return NULL;

}

int main(int argc, char **argv, char **envp)
{
  pthread_t waiter_thread1, waiter_thread2, poster_thread;
  pthread_attr_t attr;
  int status;

  util_printf("main: Initializing semaphore to 0\n");
  sem_init(&sem, 0, 0);

  /* Start two waiter thread instances */

  util_printf("main: Starting waiter thread 1\n");
  status = pthread_attr_init(&attr);
  if (status != 0)
    {
      util_printf("main: pthread_attr_init failed, status=%d\n", status);
    }

  attr.sched_priority = 8; /* Cheating because I don't want to include sched.h */

  if ((pthread_create(&waiter_thread1, &attr, waiter_func, (void*)1)) != 0)
    {
      util_printf("main: Error in thread#1 creation\n");
    }

  util_printf("main: Starting waiter thread 2\n");
  status = pthread_attr_init(&attr);
  if (status != 0)
    {
      util_printf("main: pthread_attr_init failed, status=%d\n", status);
    }

  attr.sched_priority = 4; /* Cheating because I don't want to include sched.h */

  if ((pthread_create(&waiter_thread2, &attr, waiter_func, (void*)2)) != 0)
    {
      util_printf("main: Error in thread#2 creation\n");
    }

  util_printf("main: Starting poster thread 3\n");
  status = pthread_attr_init(&attr);
  if (status != 0)
    {
      util_printf("main: pthread_attr_init failed, status=%d\n", status);
    }

  attr.sched_priority = 2; /* Cheating because I don't want to include sched.h */

  if ((pthread_create(&poster_thread, &attr, poster_func, (void*)3)) != 0)
    {
      util_printf("main: Error in thread#3 creation\n");
    }

  pthread_join(waiter_thread1, NULL);
  pthread_join(waiter_thread2, NULL);
  pthread_join(poster_thread, NULL);
  sem_destroy(&sem);
  return 0;
}
