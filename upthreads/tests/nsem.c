/***********************************************************************
 * upthreads/tests/nsem.c
 *
 *   Copyright (c) 2004, 2006, Gregory Nutt.  All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************************************/

#include "pthread.h"
#include "semaphore.h"
#include "mqueue-fcntl.h"
#include "util.h"

#ifndef NULL
# define NULL (void*)0
#endif

#define NWAITERS  8
#define NSEM_NAME "somename"

static void *waiter_func(void *parameter)
{
  int my_id = (int)parameter;
  sem_t *nsem;
  int value;
  int status;

  util_printf("Thread %d: Started\n", my_id);

  /* Open the semaphore */

  util_printf("Thread %d: Opening semaphore %s O_RDONLY\n", my_id, NSEM_NAME);
  nsem = sem_open(NSEM_NAME, O_RDONLY);
  if (nsem == SEM_FAILED)
    {
      util_printf("Thread %d: ERROR sem_open of %s failed\n", NSEM_NAME);
      return (void*)0xdeadbeef;
    }

  /* Take the semaphore */

  status = sem_getvalue(nsem, &value);
  if (status < 0)
    {
      util_printf("Thread %d: ERROR Could not get semaphore value\n", my_id);
    }
  else
    {
      util_printf("Thread %d: Initial semaphore value = %d\n", my_id, value);
    }

  util_printf("Waiting on semaphore\n", my_id);
  status = sem_wait(nsem);
  if (status != 0)
    {
      util_printf("Thread %d: ERROR sem_wait failed\n", my_id);
    }
  util_printf("Thread %d: Awakened\n", my_id);

  status = sem_getvalue(nsem, &value);
  if (status < 0)
    {
      util_printf("ERROR thread %d: Could not get semaphore value\n", my_id);
    }
  else
    {
      util_printf("Thread %d: New semaphore value = %d\n", my_id, value);
    }

  util_printf("Thread %d: Done\n", my_id);
  return (void*)0x12345678;
}

int main(int argc, char **argv, char **envp)
{
  pthread_t waiter[NWAITERS];
  pthread_attr_t attr;
  sem_t *nsem;
  void *result;
  int value;
  int status;
  int i;

  /* Open the semaphore */

  util_printf("main: Opening semaphore %s O_WRONLY|O_CREAT with value = 0\n", NSEM_NAME);
  nsem = sem_open(NSEM_NAME, O_WRONLY|O_CREAT, 0666, 0);
  if (nsem == SEM_FAILED)
    {
      util_printf("main: ERROR sem_open of %s failed\n", NSEM_NAME);
    }

  status = sem_getvalue(nsem, &value);
  if (status < 0)
    {
      util_printf("main: ERROR Could not get semaphore value\n");
    }
  else
    {
      util_printf("main: Initial semaphore value = %d\n", value);
    }

  /* Start NWAITERS waiter thread instances */

  for (i = 0; i < NWAITERS; i++)
    {
      util_printf("main: Starting waiter thread %d\n", i);
      status = pthread_attr_init(&attr);
      if (status != 0)
        {
          util_printf("main: ERROR pthread_attr_init failed, status=%d\n", status);
        }

      attr.sched_priority = 8; /* Cheating because I don't want to include sched.h */

      if ((pthread_create(&waiter[i], &attr, waiter_func, (void*)i)) != 0)
      {
        util_printf("main: ERROR pthread_create of thread#%d failed\n", i);
      }

      /* Wait a bit */

      pthread_yield();

      /* Then get the new value of the semaphore */

      status = sem_getvalue(nsem, &value);
      if (status < 0)
        {
          util_printf("main: ERROR Could not get semaphore value\n");
        }
      else
        {
          util_printf("main: New semaphore value = %d\n", value);
        }
    }

  /* Unlink the queue now.  This should have no effect until every thread
   * closes the semaphore.
   */

  status = sem_unlink(NSEM_NAME);
  if (status != 0)
    {
      util_printf("main: ERROR sem_unlink failed\n");
    }


  /* Now post the semaphore NWAITERS times */

  for (i = 0; i < NWAITERS; i++)
    {
      status = sem_post(nsem);
      if (status != 0)
        {
          util_printf("main: ERROR sem_post %d failed\n", i);
        }
      else
        {
          util_printf("main: sem_post %d succeeded\n", i);
        }

      /* Wait a bit */

      pthread_yield();

      /* Then get the new value of the semaphore */

      status = sem_getvalue(nsem, &value);
      if (status < 0)
        {
          util_printf("main: ERROR Could not get semaphore value\n");
        }
      else
        {
          util_printf("main: New semaphore value = %d\n", value);
        }
    }

  /* Now join to each of the threads to make sure that they exit as expected. */

  for (i = 0; i < NWAITERS; i++)
    {
      status = pthread_join(waiter[i], &result);
      if (status != 0)
        {
          util_printf("main: ERROR pthread_join to thread %d failed with %d\n", i, status);
        }
      else if (result != (void*)0x12345678)
        {
          util_printf("main: ERROR thread %d returned unexpected status %p\n", i, result);
        }
      else
        {
          util_printf("main: PASS thread %d terminated successfully\n", i);
        }
    }
  return 0;
}
