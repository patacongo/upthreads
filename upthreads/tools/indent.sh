#!/bin/sh
#
# This script uses the Linux 'indent' utility to re-format C source files
# to match the coding style that I use.  It differs from my coding style in that
#
# - I normally put the traiing */ of a multi-line comment on a separate line,
# - I usually align things vertically (like '='in assignments.
#

indent -nbad -bap -bbb -nbbo -nbc -bl -bl2 -bls -nbs -cbi2 -ncdw -nce -ci2 -cli0 -cp40 -ncs -nbfda -nbfde -di1 -nfc1 -fca -i2 -l80 -lp -ppi2 -lps -npcs -pmt -nprs -npsl -saf -sai -sbi2 -saw -sc -sob -nss -nut "$@"

