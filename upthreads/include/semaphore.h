
/**************************************************************************
 * upthreads/include/semaphore.c
 * POSIX semaphore declarations
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

#ifndef __SEMAPHORE_H
#  define __SEMAPHORE_H

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#  include "pthread-types.h"

/**************************************************************************
 * Public Definitions
 **************************************************************************/

/* Value returned if sem_open() fails  */

#  define SEM_FAILED   ((sem_t*)0)

/* Max value POSIX counting semaphore */

#  define SEM_MAX_VALUE 0x7fffffff

/**************************************************************************
 * Public Types
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Public Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Public Functions
 **************************************************************************/

#  ifdef __cplusplus
extern "C"
{
#  endif

/**************************************************************************
 * Function:  U_sem_init
 *
 * Description:
 * This function initializes the UNAMED semaphore sem.
 * Following a successful call to U_sem_init(), the semaophore
 * may be used in subsequent calls to U_sem_wait(), U_sem_post(),
 * and U_U_sem_trywait().  The semaphore remains usable until it
 * is destroyed.
 *
 * Only sem itself may be used for performing synchronization.
 * The result of referring to copies of sem in calls to
 * U_sem_wait(), U_U_sem_trywait(), U_sem_post(), and U_sem_destroy() is
 * undefined.
 *
 * Parameters:
 *   sem - Semaphore to be initialized
 *   pshared - Process sharing (not used)
 *   value - Semaphore initialization value
 *
 * Return Value:
 *   0, or -1 if unsuccessful.
 *
 * Assumptions:
 *
 **************************************************************************/

  int U_sem_init(U_sem_t * sem, int pshared, unsigned int value);

/**************************************************************************
 * Function:  U_sem_destroy
 *
 * Description:
 * This function is used to destroy the un-named semaphore
 * indicated by sem.  Only a semaphore that was created using
 * U_sem_init() may be destroyed using U_sem_destroy(); the
 * effect of calling U_sem_destroy() with a name semaphore
 * is undefined.  The effect of subsequent use of the semaphore
 * sem is undefined until sem is re-initialized by another
 * call to U_sem_init().
 *
 * The effect of destroying a semaphore upon which other
 * processes are currently blocked is undefined.
 *
 * Parameters:
 *   sem - Semaphore to be destroyed.
 *
 * Return Value:
 *   0, or -1 if unsuccessful.
 *
 * Assumptions:
 *
 **************************************************************************/

  int U_sem_destroy(U_sem_t * sem);

/**************************************************************************
 * Function:  U_sem_open
 *
 * Description:
 * This function establishes a connection between named
 * semaphores and a task.  Following a call to U_sem_open()
 * with the semaphore name, the task may reference the
 * semaphore associated with name using the address
 * returned by this call.  The semaphore may be used in
 * subsequent calls to U_sem_wait(), U_sem_trywait(), and
 * U_sem_post().  The semaphore remains usable until the
 * semaphore is closed by a successful call to U_sem_close().
 *
 * If a task makes multiple calls to U_sem_open() with the
 * same name, then the same semaphore address is returned
 * (provided there have been no calls to U_sem_unlink()).
 *
 * Parameters:
 *   name  - Semaphore name
 *   oflag - Semaphore creation options.  This may either
 *     or both of the following bit settings.
 *     oflag = 0:  Connect to the semaphore only if it
 *        already exists.
 *     oflag = O_CREAT:  Connect to the semaphore if it
 *        exists, otherwise create the semaphore.
 *     oflag = O_CREAT|O_EXCL:  Create a new semaphore
 *        unless one of this name already exists.
 *   ... - Optional parameters.  When the O_CREAT flag is specified,
 *        two optional parameters are expected:
 *        1. unsigned int mode (ignored), and
 *        2. unsigned int value.  This initial value of the semaphore.
 *           valid initial values of the semaphore must be less than
 *           or equal to SEM_MAX_VALUE.
 *
 * Return Value:
 *   A pointer to U_sem_t or -1 if unsuccessful.
 *
 * Assumptions:
 *
 **************************************************************************/

  U_sem_t *U_sem_open(const char *name, int oflag, ...);

/**************************************************************************
 * Function:  U_sem_close
 *
 * Description:
 * This function is called to indicate that the calling task
 * is finished with the specified named semaphore, sem.  The
 * U_sem_close() deallocates any system resources allocated by
 * the system for this named semaphore.
 *
 * If the semaphore has not been removed with a call to
 * U_sem_unlink(), then U_sem_close() has no effect on the
 * named semaphore.  However, when the named semaphore has
 * been fully unlinked, the semaphore will vanish when the
 * last task closes it.
 *
 * Parameters:
 *  sem - semaphore descriptor
 *
 * Return Value:
 *  0, or -1 if unsuccessful.
 *
 * Assumptions:
 *   - Care must be taken to avoid risking the deletion of
 *     a semaphore that another calling task has already
 *     locked.
 *   - U_sem_close must not be called for an un-named semaphore
 *
 **************************************************************************/

  int U_sem_close(U_sem_t * sem);

/**************************************************************************
 * Function:  U_sem_unlink
 *
 * Description:
 * This function removes the semaphore named by the input
 * parameter "name."  If the semaphore named by "name" is
 * currently referenced by other processes, the U_sem_unlink()
 * will have no effect on the state of the semaphore.  If
 * one or more processes have the semaphore open when U_sem_unlink()
 * is called, destruction of the semaphore will be postponed
 * until all references to the semaphore have been destroyed
 * by calls of U_sem_close().
 *
 * Parameters:
 *   name - Semaphore name
 *
 * Return Value:
 *  0, or -1 if unsuccessful.
 *
 * Assumptions:
 *
 **************************************************************************/

  int U_sem_unlink(const char *name);

/**************************************************************************
 * Function:  U_sem_wait
 *
 * Description:
 * This function attempts to lock the semaphore referenced by
 * sem.  If the semaphore value is (<=) zero, then the calling
 * task will not return until it successfully acquires the lock.
 *
 * Parameters:
 *   sem - Semaphore descriptor.
 *
 * Return Value:
 *   0, or -1 if unsuccessful
 *
 * Assumptions:
 *
 **************************************************************************/

  int U_sem_wait(U_sem_t * sem);

/**************************************************************************
 * Function:  U_sem_trywait
 *
 * Description:
 * This function locks the specified semaphore only if the
 * semaphore is currently not locked.  Otherwise, it locks
 * the semaphore.  In either case, the call returns without
 * blocking.
 *
 * Parameters:
 *   sem - the semaphore descriptor
 *
 * Return Value:
 *   0 or -1 if unsuccessful (probably because the semaphore is not
 *   available).
 *
 * Assumptions:
 *
 **************************************************************************/

  int U_sem_trywait(U_sem_t * sem);

/**************************************************************************
 * Function:  U_sem_post
 *
 * Description:
 * When a task has finished with a semaphore, it will call
 * U_sem_post().  This function unlocks the semaphore referenced
 * by sem by performing the semaphore unlock operation on that
 * semaphore.
 *
 * If the semaphore value resulting from this operation is positive,
 * then no tasks were blocked waiting for the semaphore to
 * become unlocked; the semaphore is simply incremented.
 *
 * If the value of the semaphore resulting from this operation is
 * zero, then one of the tasks blocked waiting for the
 * semaphore shall be allowed to return successfully from its
 * call to U_sem_wait().
 *
 * Parameters:
 *   sem - Semaphore descriptor
 *
 * Return Value:
 *   0 or -1 if unsuccessful
 *
 * Assumptions:
 *
 **************************************************************************/

  int U_sem_post(U_sem_t * sem);

/**************************************************************************
 * Function:  U_sem_getvalue
 *
 * Description:
 * This function updates the location referenced by sval
 * argument to have the value of the semaphore referenced by
 * sem without effecting the state of the semaphore.  The
 * updated value represents the actual semaphore value that
 * occurred at some unspecified time during the call, but may
 * not reflect the actual value of the semaphore when it is
 * returned to the calling task.
 *
 * If sem is locked, the value return by U_sem_getvalue() will
 * either be zero or a negative number whose absolute value
 * represents the number of tasks waiting for the semaphore.
 *
 * Parameters:
 *   sem - Semaphore descriptor
 *   sval - Buffer by which the value is returned
 *
 * Return Value:
 *   0, or -1 if unsuccessful
 *
 * Assumptions:
 *
 **************************************************************************/

  int U_sem_getvalue(U_sem_t * sem, int *sval);

#  ifdef __cplusplus
}
#  endif

/* A separate namespace is used for all APIs in this libpthread library.
 * This was done so that this pthread library will be compatible with
 * glibc and uCibc which have their own pthread support and which reference
 * the pthread APIs internally.  If we don't change the names, then
 * the will reference the wrong implementation!
 *
 * But to make the user programming environment a little more standard,
 * the following macros will correct the namespace for all programs
 * that include this header file.
 */

/* Types */

#  define sem_t              U_sem_t

/* APIs */

#  define sem_init(a,b,c)    U_sem_init(a,b,c)
#  define sem_destroy(a)     U_sem_destroy(a)
#  define sem_open(a,b...)   U_sem_open(a, ##b)
#  define sem_close(a)       U_sem_close(a)
#  define sem_unlink(a)      U_sem_unlink(a)
#  define sem_wait(a)        U_sem_wait(a)
#  define sem_trywait(a)     U_sem_trywait(a)
#  define sem_post(a)        U_sem_post(a)
#  define sem_getvalue(a,b)  U_sem_getvalue(a,b)

#endif                          /* __SEMAPHORE_H */
