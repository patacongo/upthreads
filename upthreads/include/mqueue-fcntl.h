
/**************************************************************************
 * mqueue-fcntl.h
 * Use this is fcntl.y is not available
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

#ifndef __MQUEUE_FCNTL_H
#  define __MQUEUE_FCNTL_H

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#  include "pthread-config.h"
#  ifdef CCONFIG_HAVE_FCNTL_H
#    include <fcntl.h>
#  else

/**************************************************************************
 * Public Definitions
 **************************************************************************/

#    define O_ACCMODE          0003
#    define O_RDONLY             00
#    define O_WRONLY             01
#    define O_RDWR               02
#    define O_CREAT            0100
                                /* not fcntl */
#    define O_EXCL             0200
                                /* not fcntl */
#    define O_NOCTTY           0400
                                /* not fcntl */
#    define O_TRUNC           01000
                                /* not fcntl */
#    define O_APPEND          02000
#    define O_NONBLOCK        04000
#    define O_NDELAY        O_NONBLOCK
#    define O_SYNC           010000
#    define O_FSYNC          O_SYNC
#    define O_ASYNC          020000

#    ifdef __USE_GNU
#      define O_DIRECT        040000
                                /* Direct disk access.  */
#      define O_DIRECTORY    0200000
                                /* Must be a directory.  */
#      define O_NOFOLLOW     0400000
                                /* Do not follow links.  */
#      define O_NOATIME     01000000
                                /* Do not set atime.  */
#    endif

#    if defined __USE_POSIX199309 || defined __USE_UNIX98
#      define O_DSYNC        O_SYNC
                                /* Synchronize data.  */
#      define O_RSYNC        O_SYNC
                                /* Synchronize read operations.  */
#    endif

#  endif                               /* CONFIG_HAVE_ERRNO_H */
#endif                                 /* __MQUEUE_FCNTL_H */
