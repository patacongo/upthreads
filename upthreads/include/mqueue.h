
/**************************************************************************
 * mqueue.h
 * POSIX Message Queue Declarations
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

#ifndef __MQUEUE_H
#  define __MQUEUE_H

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#  include "pthread-config.h"

/**************************************************************************
 * Public Definitions
 **************************************************************************/

/* The range of valid message priorities is 0..MQ_PRIO_MAX */

#  define MQ_PRIO_MAX  255

/**************************************************************************
 * Public Types
 **************************************************************************/

#  include "pthread-types.h"

/**************************************************************************
 * Public Variables
 **************************************************************************/

/**************************************************************************
 * Public Function Prototypes
 **************************************************************************/

#  ifdef __cplusplus
extern "C"
{
#  endif

/**************************************************************************
 * Function:  U_mq_open
 *
 * Description:
 * This function establish a connection between a named
 * message queue and the calling task.  After a successful
 * call of mq_open(), the task can reference the message
 * queue using the address returned by the call.  The message
 * queue remains usable until it is closed by a successful
 * call to U_mq_close().
 *
 * Parameters:
 *   mq_name - Name of the queue to open
 *   oflags - open flags (see os_struct.h)
 *   ...    - Optional parameters.  When the O_CREAT flag is specified,
 *            two optional parameters are expected:
 *            1. osMode_t mode (ignored), and
 *            2. struct U_mq_attr *attr.  The mq_maxmsg attribute is used
 *               at the time that the message queue is created to determine
 *               the maximum number of messages that may be placed in the
 *               message queue.
 *
 * Return Value:
 *   A message queue descriptor or -1 (ERROR)
 *
 * Assumptions:
 *
 **************************************************************************/

  U_mqd_t U_mq_open(const char *mq_name, int oflags, ...);

/************************************************************
 * Function:  U_mq_close
 *
 * Description:
 * This function is used to indicate that the calling task is
 * finished with the specified message queued mqdes.  The
 * U_mq_close() deallocates any system resources allocated by the
 * system for use by this task for its message queue.
 *
 * If the calling task has attached a notification to the message
 * queue via this mqdes, this attachment will be removed and the
 * message queue is available for another process to attach a
 * notification.
 *
 * Parameters:
 *   mqdes - Message queue descriptor.
 *
 * Return Value:
 *   0 (0) if the message queue is closed successfully,
 *   otherwise, -1 (-1).
 *
 * Assumptions:
 * - The behavior of a task that is blocked on either a mq_send()
 *   or mq_receive is undefined when U_mq_close() is called.
 * - The results of using this message queue descriptor after a
 *   a successful return from U_mq_close() is undefined.
 *
 ************************************************************/

  int U_mq_close(U_mqd_t mqdes);

/************************************************************
 * Function:  U_mq_unlink
 *
 * Description:
 * This function removes the message queue named by "mq_name."
 * If one or more tasks have the message queue open when
 * mq_unlink() is called, removal of the message queue is
 * postponed until all references to the message queue have
 * been closed.
 * 
 * Parameters:
 *   mq_name - Name of the message queue
 *
 * Return Value:
 *   None
 *
 * Assumptions:
 *
 ************************************************************/

  int U_mq_unlink(const char *mq_name);

/************************************************************
 * Function:  U_mq_send
 *
 * Description:
 * This function adds the specificied message message (msg) to
 * the message queue (mqdes).  The "msglen" parameter specifies
 * the length of the message in bytes pointed to by "msg."
 * This length must not exceed the maximum message length from
 * the mq_getattr().
 *
 * If the message queue is not full, mq_send() will in the
 * message in the message queue at the position indicated by
 * the "msgprio" argrument.  Messages with higher priority
 * will be inserted before lower priority messages.  The
 * value of "msgprio" must not exceed MQ_PRIO_MAX.
 *
 * If the specified message queue is full and O_NONBLOCK is
 * not set in the message queue, then mq_send() will block
 * until space becomes available to the queue the message.
 * 
 * If the message queue is full and osNON_BLOCK is set, the
 * message is not queued and -1 is returned.
 * 
 * Parameters:
 *   mqdes - Message queue descriptor
 *   msg - Message to send
 *   msglen - The length of the message in bytes
 *   msgprio - The priority of the message
 *
 * Return Value:
 *   None
 *
 * Assumptions/restrictions:
 *
 ************************************************************/

  int U_mq_send(U_mqd_t mqdes, const void *msg,
                unsigned int msglen, int msgprio);

/************************************************************
 * Function:  U_mq_receive
 *
 * Description:
 * This function receives the oldest of the highest priority
 * messages from the message queue specified by "mqdes."  If
 * the size of the buffer in bytes (msglen) is less than the
 * the "mq_msgsize" attribute of the message queue, mq_receive
 * will return an error.  Otherwise, the select message is
 * removed from the queue and copied to "msg."
 *
 * If the message queue is empty and O_NONBLOCK was not set,
 * mq_receive() will block until a message is added to the
 * message queue.  If more than one task is waiting to
 * receive a message, only the task with the highest priority
 * that has waited the longest will be unblocked.
 *
 * If the queue is empty and O_NONBLOCK is set, -1 will
 * be returned.
 *
 * Parameters:
 *   mqdes - Message Queue Descriptor
 *   msg - Buffer to receive the message
 *   msglen - Size of the buffer in bytes
 *   msgprio - If not NULL, the location to store message
 *      priority.
 *
 * Return Value:
 *   Length of the selected message in bytes, otherwise -1
 *   (-1).
 *
 * Assumptions:
 *
 ************************************************************/

  int U_mq_receive(U_mqd_t mqdes, void *msg, unsigned int msglen, int *msgprio);

/************************************************************
 * Function:  u_mq_notify
 *
 * Description:
 * If "notification" is not NULL, this function connects the
 * task with the message queue such that the specified signal
 * will be sent to the task whenever the message changes from
 * empty to non-empty.  One one notification can be attached
 * to a message queue.
 *
 * If "notification" is NULL, the attached notification is
 * detached (if it was held by the calling task) and the queue
 * is available to attach another notification.
 *
 * When the notification is sent to the registered process, its
 * registration will be removed.  The message queue will then be
 * available for registration.
 *
 * Parameters:
 *   mqdes - Message queue descriptor
 *   notification - Real-time signal structure containing:
 *      sigev_notify - Ignored
 *      sigev_signo - The signo to use for the notification
 *      sigev_value - Value associated with the signal
 *
 * Return Value:
 *   None
 *
 * Assumptions:
 *
 * POSIX Compatibility:
 *   int mq_notify(U_mqd_t mqdes, const struct sigevent *notification);
 *
 *   The notification will be sent to the registered task even if another
 *   task is waiting for the message queue to become non-empty.  This is
 *   inconsistent with the POSIX specification which says, "If a process
 *   has registered for notification of message a arrival at a message
 *   queue and some process is blocked in mq_receive() waiting to receive
 *   a message when a message arrives at the queue, the arriving message
 *   message shall satisfy mq_receive()... The resulting behavior is as if
 *   the message queue remains empty, and no notification shall be sent."
 *
 ************************************************************/

#  ifdef CONFIG_HAVE_SIGNAL_H
  int U_mq_notify(U_mqd_t mqdes, const struct U_sigevent *notification);
#  endif

/************************************************************
 * Function:  U_mq_setattr
 *
 * Description:
 * This function sets the attributes associated with the
 * specified message queue "mqdes."  Only the "O_NONBLOCK"
 * bit of the "mq_flags" can be changed.
 *
 * If "oldMqStat" is non-null, mq_setattr() will store the
 * previous message queue attributes at that location (just
 * as would have been returned by mq_getattr()).
 *
 * Parameters:
 *   mqdes - Message queue descriptor
 *   attr - New attributes
 *   old_attr - Old attributes
 *
 * Return Value:
 *   0 (0) if attributes are set successfully, otherwise
 *   -1 (-1).
 *
 * Assumptions:
 *
 ************************************************************/

  int U_mq_setattr(U_mqd_t mqdes, const struct U_mq_attr_s *attr,
                   struct U_mq_attr_s *old_attr);

/************************************************************
 * Function:  U_mq_getattr
 *
 * Description:
 * This functions gets status information and attributes
 * associated with the specified message queue.
 *
 * Parameters:
 *   mqdes - Message queue descriptor
 *   attr - Buffer in which to return attributes (see
 *      os_struct.h for description.
 *
 * Return Value:
 *   0 (0) if attributes provided, -1 (-1) otherwise.
 *
 * Assumptions:
 *
 ************************************************************/

  int U_mq_getattr(U_mqd_t mqdes, struct U_mq_attr_s *attr);

#  ifdef __cplusplus
}
#  endif

/**************************************************************************
 * Namespace Fixup
 **************************************************************************/

/* A separate namespace is used for all APIs in this libpthread library.
 * This was done so that this pthread library will be compatible with
 * glibc and uCibc which have their own pthread support and which reference
 * the pthread APIs internally.  If we don't change the names, then
 * the will reference the wrong implementation!
 *
 * But to make the user programming environment a little more standard,
 * the following macros will correct the namespace for all programs
 * that include this header file.
 */

/* Types */

#  define mq_attr              U_mq_attr_s
#  define sigevent             U_sigevent_s
#  define mqd_t                U_mqd_t

/* APIs */

#  define mq_open(a,b...)      U_mq_open(a, ##b)
#  define mq_close(a)          U_mq_close(a)
#  define mq_unlink(a)         U_mq_unlink(a)
#  define mq_send(a,b,c,d)     U_mq_send(a,b,c,d)
#  define mq_receive(a,b,c,d)  U_mq_receive(a,b,c,d)
#  ifdef CONFIG_HAVE_SIGNAL_H
#    define mq_notify(a,b)      U_mq_notify(a,b)
#  endif
#  define mq_setattr(a,b,c,d)  U_mq_setattr(a,b,c,d)
#  define mq_getattr(a,b)      U_mq_getattr(a,b)

#endif                          /* __MQUEUE_H */
