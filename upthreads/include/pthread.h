
/**************************************************************************
 * pthread.h
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

#ifndef __PTHREAD_H
#  define __PTHREAD_H

/**************************************************************************
 * Included Files
 **************************************************************************/

#  include <stddef.h>
#  include "pthread-config.h"
#  include "pthread-types.h"

/**************************************************************************
 * Public Definitions
 **************************************************************************/

/* Select how many slots (keys) will be set aside for thread specific
 * data.  If CONFIG_NKEYS is not defined, then the system default will
 * be used.  You can eliminate all thread specific data support by
 * setting CONFIG_NKEYS to zero.
 */

#  ifndef CONFIG_NKEYS
#    define PTHREAD_KEYS_MAX PTHREAD_DEFAULT_KEYS_MAX
#  else
#    define PTHREAD_KEYS_MAX CONFIG_NKEYS
#  endif

/* Cancellation states */

#  define PTHREAD_CANCEL_ENABLE   (0)
#  define PTHREAD_CANCEL_DISABLE  (1)

/* Thread return value when it is canceled */

#  define PTHREAD_CANCELED ((void*)-1)

/**************************************************************************
 * Public Types
 **************************************************************************/

/**************************************************************************
 * Public Variables
 **************************************************************************/

/**************************************************************************
 * Public Function Prototypes
 **************************************************************************/

#  ifdef __cplusplus
extern "C"
{
#  endif

/**************************************************************************/

/* pthread attributes */

/* Create / destruction */

  int U_pthread_attr_init(U_pthread_attr_t * attr);
  int U_pthread_attr_destroy(U_pthread_attr_t * attr);

/* Stack size */

#  ifdef CONFIG_MEMORY_ALLOCATOR
  int U_pthread_attr_getstacksize(const U_pthread_attr_t * attr,
                                  size_t * stacksize);
  int U_pthread_attr_setstacksize(U_pthread_attr_t * attr, size_t stacksize);
#  endif

/* Execution priority */

  int U_pthread_attr_setschedparam(U_pthread_attr_t * attr,
                                   const struct sched_param *param);
  int U_pthread_attr_getschedparam(const U_pthread_attr_t * attr,
                                   struct sched_param *param);

/* Detached state */

  int U_pthread_attr_setdetachstate(U_pthread_attr_t * attr, int detachstate);
  int U_pthread_attr_getdetachstate(const U_pthread_attr_t * attr,
                                    int *detachstate);

/**************************************************************************/

/* pthread controls */

/* Creation / exit */

  int U_pthread_create(U_pthread_t * thread, U_pthread_attr_t * attr,
                       void *(*start_routine) (void *), void *arg);
  void U_pthread_exit(void *retval);
  int U_pthread_cancel(U_pthread_t thread);
  int U_pthread_setcancelstate(int state, int *oldstate);

/* Pthread return values */

  int U_pthread_detach(U_pthread_t thread);
  int U_pthread_join(U_pthread_t thread, void **thread_return);

/* Scheduling */

  void U_pthread_yield(void);

/* Thread identity */

  U_pthread_t U_pthread_self(void);
  int U_pthread_equal(U_pthread_t thread1, U_pthread_t thread2);

/* Non-standard controls for locking/unlocking the schedule.  UPthreads
 * are non-pre-emptible.  When the the scheduler is locked, you can make
 * higher priority threads ready to run (e.g., via pthread_mutex_unlock())
 * but block thoses threads from running until you explicitly suspend or
 * call U_pthread_sched_unlock().
 */

  int U_pthread_sched_lock(void);
  int U_pthread_sched_unlock(void);

/**************************************************************************/

/* Thread specific data */

  int U_pthread_key_create(U_pthread_key_t * key,
                           void (*destr_function) (void *));
  int U_pthread_key_delete(U_pthread_key_t key);
  void *U_pthread_getspecific(U_pthread_key_t key);
  int U_pthread_setspecific(U_pthread_key_t key, void *value);

/**************************************************************************/

/* Mutexes */

/* Mutex attributes */

  int U_pthread_mutexattr_init(U_pthread_mutexattr_t * attr);
  int U_pthread_mutexattr_destroy(U_pthread_mutexattr_t * attr);
  int U_pthread_mutexattr_settype(U_pthread_mutexattr_t * attr, int type);

/* Creation / destruction */

  int U_pthread_mutex_init(U_pthread_mutex_t * mutex,
                           const U_pthread_mutexattr_t * mutexattr);
  int U_pthread_mutex_destroy(U_pthread_mutex_t * mutex);

/* locking / unlocking */

  int U_pthread_mutex_lock(U_pthread_mutex_t * mutex);
  int U_pthread_mutex_trylock(U_pthread_mutex_t * mutex);
  int U_pthread_mutex_unlock(U_pthread_mutex_t * mutex);

/**************************************************************************/

/* Condition variables */

/* Condition variable attributes */

  int U_pthread_condattr_init(U_pthread_condattr_t * attr);
  int U_pthread_condattr_destroy(U_pthread_condattr_t * attr);

/* Creation / destruction */

  int U_pthread_cond_init(U_pthread_cond_t * cond,
                          const U_pthread_condattr_t * cond_attr);
  int U_pthread_cond_destroy(U_pthread_cond_t * cond);

/* Signaling / Waiting */

  int U_pthread_cond_signal(U_pthread_cond_t * cond);
  int U_pthread_cond_broadcast(U_pthread_cond_t * cond);
  int U_pthread_cond_wait(U_pthread_cond_t * cond, U_pthread_mutex_t * mutex);

#  ifdef CONFIG_PTHREAD_TIMER
  int U_pthread_cond_timedwait(U_pthread_cond_t * cond,
                               U_pthread_mutex_t * mutex,
                               const struct timespec *abstime);

/* This is non-standard time utilities that it may be uses with
 * pthread_cond_timedwait.
 */

  void U_pthread_time(struct timespec *time);
#  endif

/* Once Mechanism */

  int U_pthread_once(U_pthread_once_t * once_control,
                     void (*init_routine) (void));

#  ifdef __cplusplus
}
#  endif

/**************************************************************************
 * Namespace Fixup
 **************************************************************************/

/* A separate namespace is used for all APIs in this libpthread library.
 * This was done so that this pthread library will be compatible with
 * glibc and uCibc which have their own pthread support and which reference
 * the pthread APIs internally.  If we don't change the names, then
 * the will reference the wrong implementation!
 *
 * But to make the user programming environment a little more standard,
 * the following macros will correct the namespace for all programs
 * that include this header file.
 */

/* Types */

#  define pthread_t            U_pthread_t
#  define pthread_attr_t       U_pthread_attr_t
#  define pthread_mutex_t      U_pthread_mutex_t
#  define pthread_mutexattr_t  U_pthread_mutexattr_t
#  define pthread_cond_t       U_pthread_cond_t
#  define pthread_condattr_t   U_pthread_condattr_t
#  define pthread_key_t        U_pthread_key_t
#  define pthread_once_t       U_pthread_once_t

/* pthread attributes */

#  define pthread_attr_init(a)             U_pthread_attr_init(a)
#  define pthread_attr_destroy(a)          U_pthread_attr_destroy(a)
#  ifdef CONFIG_MEMORY_ALLOCATOR
#    define pthread_attr_getstacksize(a,b)   U_pthread_attr_getstacksize(a,b)
#    define pthread_attr_setstacksize(a,b)   U_pthread_attr_setstacksize(a,b)
#  endif
#  define pthread_attr_setschedparam(a,b)  U_pthread_attr_setschedparam(a,b)
#  define pthread_attr_getschedparam(a,b)  U_pthread_attr_getschedparam(a,b)
#  define pthread_attr_setdetachstate(a,b) U_pthread_attr_setdetachstate(a,b)
#  define pthread_attr_getdetachstate(a,b) U_pthread_attr_getdetachstate(a,b)

/* pthread controls */

#  define pthread_create(a,b,c,d)          U_pthread_create(a,b,c,d)
#  define pthread_exit(a)                  U_pthread_exit(a)
#  define pthread_cancel(a)                U_pthread_cancel(a)
#  define pthread_setcancelstate(a,b)      U_pthread_setcancelstate(a,b)
#  define pthread_detach(a)                U_pthread_detach(a)
#  define pthread_join(a,b)                U_pthread_join(a,b)
#  define pthread_yield()                  U_pthread_yield()
#  define pthread_self()                   U_pthread_self()
#  define pthread_equal(a,b)               U_pthread_equal(a,b)
#  define pthread_sched_lock()             U_pthread_sched_lock()
#  define pthread_sched_unlock()           U_pthread_sched_unlock()

/* thread specific data */

#  define pthread_key_create(a,b)          U_pthread_key_create(a,b)
#  define pthread_key_delete(a)            U_pthread_key_delete(a)
#  define pthread_getspecific(a)           U_pthread_getspecific(a)
#  define pthread_setspecific(a,b)         U_pthread_setspecific(a,b)

/* Mutexes */

#  define pthread_mutexattr_init(a)        U_pthread_mutexattr_init(a)
#  define pthread_mutexattr_destroy(a)     U_pthread_mutexattr_destroy(a)
#  define pthread_mutexattr_settype(a,b)   U_pthread_mutexattr_settype(a,b)
#  define pthread_mutex_init(a,b)          U_pthread_mutex_init(a,b)
#  define pthread_mutex_destroy(a)         U_pthread_mutex_destroy(a)
#  define pthread_mutex_lock(a)            U_pthread_mutex_lock(a)
#  define pthread_mutex_trylock(a)         U_pthread_mutex_trylock(a)
#  define pthread_mutex_unlock(a)          U_pthread_mutex_unlock(a)

/* Condition variables */

#  define pthread_condattr_init(a)         U_pthread_condattr_init(a)
#  define pthread_condattr_destroy(a)      U_pthread_condattr_destroy(a)
#  define pthread_cond_init(a,b)           U_pthread_cond_init(a,b)
#  define pthread_cond_destroy(a)          U_pthread_cond_destroy(a)
#  define pthread_cond_signal(a)           U_pthread_cond_signal(a)
#  define pthread_cond_broadcast(a)        U_pthread_cond_broadcast(a)
#  define pthread_cond_wait(a,b)           U_pthread_cond_wait(a,b)

#  ifdef CONFIG_PTHREAD_TIMER
#    define pthread_cond_timedwait(a,b,c)    U_pthread_cond_timedwait(a,b,c)
#    define pthread_time(a)                  U_pthread_time(a)
#  endif

/* Once Mechanism */

#  define pthread_once(a,b)                U_pthread_once(a,b)

#endif                          /* __PTHREAD_H */
