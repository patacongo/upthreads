
/**************************************************************************
 * pthread-types.h
 * Internal, non-standard pthread definitions.  These have to be exposed
 * to the user, but for portability reasons, should not be referenced by
 * user programs.  User programs should not include this file explicitly...
 * use pthread.h instead.
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

#ifndef __PTHREAD_TYPES_H
#  define __PTHREAD_TYPES_H

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#  include "pthread-config.h"

/**************************************************************************
 * Public Definitions
 **************************************************************************/

/* pthread priority is set using pthread_attr_setparam().  The
 * system sched_get_priority_min and sched_get_priority_max should
 * not be used.  Rather, the following, non-standard defintion
 * should be used:
 */

#  define PTHREAD_PRIORITY_MAX (CONFIG_NPRIORITY_LEVELS-1)
#  define PTHREAD_PRIORITY_MIN  0

/* This is the default value of PTHREAD_MAX_KEYS that is used when
 * CONFIG_NKEYS is not defined.
 */

#  define PTHREAD_DEFAULT_KEYS_MAX 8

/**************************************************************************
 * Public Types
 **************************************************************************/

/* This is the internal representation of the thread handle. */

typedef void *U_pthread_t;

/* This structure represents the pthread attributes (used with
 * pthread_create().  The user should not acces this structure directly
 * because of portability and maintainability issues.  Instead, the
 * pthread_attr_* APIs should be used.
 */

struct U_pthread_attr_s
  {
    unsigned int sched_priority;        /* Execution priority of thread */
    int detachstate;            /* true: thread will start in the detached
                                 * state */
    unsigned int stacksize;     /* Size of stack used by the thread */
  };

typedef struct U_pthread_attr_s U_pthread_attr_t;

/**************************************************************************
 * mutexes
 */

/* This structure represents the mutex attributes used with
 * pthread_mutex_init.  The user should not access this structure directly
 * because of portability and maintainability issues.  Instead, the
 * pthread_mutexattr_* APIs should be used.
 */

struct U_pthread_mutexattr_s
  {
    int type;                   /* Type of the mutex */
  };

typedef struct U_pthread_mutexattr_s U_pthread_mutexattr_t;

/* Mutex types.  */

enum
  {
    PTHREAD_MUTEX_TIMED_NP,
    PTHREAD_MUTEX_RECURSIVE_NP,
    PTHREAD_MUTEX_ERRORCHECK_NP,
    PTHREAD_MUTEX_ADAPTIVE_NP,
    PTHREAD_MUTEX_NORMAL = PTHREAD_MUTEX_TIMED_NP,
    PTHREAD_MUTEX_RECURSIVE = PTHREAD_MUTEX_RECURSIVE_NP,
    PTHREAD_MUTEX_ERRORCHECK = PTHREAD_MUTEX_ERRORCHECK_NP,
    PTHREAD_MUTEX_DEFAULT = PTHREAD_MUTEX_NORMAL
  };

/* This is the internal representation of the mutex handle.  The
 * user should not reference the contents of this structure because it
 * is subject to change.
 */

struct U_pthread_mutex_s
  {
    int locked;                 /* Greater than 0: mutex is locked */
    int type;                   /* Type of the mutex */
    U_pthread_t holder;         /* Holder of the mutex */
  };

typedef struct U_pthread_mutex_s U_pthread_mutex_t;

/* Mutex initializers.  */

#  define PTHREAD_MUTEX_INITIALIZER    \
  {                                  \
    .locked = 0,                     \
    .type   = PTHREAD_MUTEX_TIMED_NP \
  }

#  define PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP \
  {                                            \
    .locked = 0,                               \
    .type   = PTHREAD_MUTEX_RECURSIVE_NP       \
  }

/**************************************************************************
 * condition variables
 */

/* This is the internal representation of the condition variable
 * handle.  The user should not reference the contents of this structure
 * because it is subject to change.
 */

struct U_pthread_cond_s
  {
    int signaled;               /* 1: Condition has been signaled */
  };

typedef struct U_pthread_cond_s U_pthread_cond_t;

/* This structure represents the condition variable attributes (used with
 * pthread_cond_init.  The user should not access this structure directly
 * because of portability and maintainability issues.  Instead, the
 * pthread_condattr_* APIs should be used.
 */

struct U_pthread_condattr_s
  {
    void *dummy;
  };

typedef struct U_pthread_condattr_s U_pthread_condattr_t;

/**************************************************************************
 * thread specific data
 */

typedef unsigned int U_pthread_key_t;

/**************************************************************************/

/* This is the internal representation of the pthread_once control
 */

#  define PTHREAD_ONCE_INIT 0
typedef int U_pthread_once_t;

/**************************************************************************/

/* Message queue attributes */

struct U_mq_attr_s
  {
    unsigned int mq_maxmsg;     /* Max number of messages in queue */
    unsigned int mq_msgsize;    /* Max message size */
    unsigned int mq_flags;      /* Queue flags */
    unsigned int mq_curmsgs;    /* Number of messages currently in queue */
  };

/**************************************************************************/

/* The following is used to attach a signal to a message queue
 * to notify a task when a message is available on a queue
 */

#  ifdef CONFIG_HAVE_SIGNAL_H
struct U_sigevent_s
  {
    int sigev_signo;
    union sigval sigev_value;
    int sigev_notify;
  };
#  endif

/**************************************************************************/

typedef void *U_mqd_t;

/**************************************************************************/

/* This is the general semaphore structure. */

struct U_sem_s
  {
    int sem_count;              /* >0 -> Num counts available */
    /* <0 -> Num tasks waiting for semaphore */
  };

typedef struct U_sem_s U_sem_t;

/**************************************************************************
 * POSIX system structures
 */

#  ifdef CONFIG_HAVE_TIME_H
struct timespec;                /* Forward reference.  User's should include
                                 * time.h */

#  else
struct timespec
  {
    int tv_sec;                 /* Seconds */
    int tv_nsec;                /* Nanoseconds */
  };
#  endif

/* If the system does not provide struct sched_param through sched.h, we
 * will make our own.
 */

#  ifdef CONFIG_HAVE_SCHED_H
struct sched_param;             /* Forward reference.  User's should include
                                 * sched.h */
#  else
struct sched_param
  {
    int sched_priority;
  };
#  endif

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Public Function Prototypes
 **************************************************************************/

#endif                                 /* __PTHREAD_TYPES_H */
