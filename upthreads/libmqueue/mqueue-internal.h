
/**************************************************************************
 * mqueue-internal
 * Definitions used only within libmqueue
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

#ifndef __MQUEUE_INTERNAL_H
#  define __MQUEUE_INTERNAL_H 1

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#  include "pthread-types.h"
#  include "pthread-internal.h"

/**************************************************************************
 * Public Definitions
 **************************************************************************/

#  if !defined(CONFIG_MEMORY_ALLOCATOR)
#    undef CONFIG_MQUEUE_PREALLOCATED
#    define CONFIG_MQUEUE_PREALLOCATED 1
#  endif

#  if defined(CONFIG_MQUEUE_PREALLOCATED)
#    undef CONFIG_MQUEUE_DEFAULT_MSGSIZE
#    ifndef CONFIG_MQUEUE_NMESSAGES
#      define CONFIG_MQUEUE_NMESSAGES 32
#    endif
#    ifndef CONFIG_MQUEUE_MSGSIZE
#      define CONFIG_MQUEUE_MSGSIZE 32
#    endif
#    ifndef CONFIG_MQUEUE_NQUEUES
#      define CONFIG_MQUEUE_NQUEUES 8
#    endif
#    ifndef CONFIG_MQUEUE_NAMELEN
#      define CONFIG_MQUEUE_NAMELEN 16
#    endif
#  else
#    ifndef CONFIG_MQUEUE_DEFAULT_MSGSIZE
#      define CONFIG_MQUEUE_DEFAULT_MSGSIZE 32
#    endif
#    undef CONFIG_MQUEUE_NMESSAGES
#  endif

#  ifndef CONFIG_MQUEUE_DEFAULT_MAXMSG
#    define CONFIG_MQUEUE_DEFAULT_MAXMSG 32
#  endif

/**************************************************************************
 * Public Types
 **************************************************************************/

/* This is the internal representation of the message queue
 * dsccriptor.  The user should not reference the contents of this structure
 * because it is subject to change.
 */
struct U_mqd_internal_s
  {
    struct U_mqueue_s *mqueue;  /* Pointer to associated message queue */
    unsigned int oflags;        /* Flags set when message queue was opened */
  };
typedef struct U_mqd_internal_s U_mqd_internal_t;

/* This structure describes one buffered POSIX message. */

struct U_mqueue_message_s
  {
    unsigned char type;         /* (Used to manage allocations) */

    unsigned char priority;     /* priority of message */
    unsigned char msglen;       /* Message data length */
    unsigned char pad;          /* Not used */

#  ifndef CONFIG_MQUEUE_MSGSIZE
    unsigned char *mail;
#  else
    unsigned char mail[CONFIG_MQUEUE_MSGSIZE];
#  endif
  };
typedef struct U_mqueue_message_s U_mqueue_message_t;

/* This structure defines a message queue */

struct U_mqueue_s
  {
    U_pthread_queue_t msglist;  /* Prioritized message list */
    short maxmsgs;              /* Maximum number of messages in the queue */
    short nconnections;         /* Number of connections to message queue */
    short nnfull_waiters;       /* Number tasks waiting for not full */
    short nnempty_waiters;      /* Number tasks waiting for not empty */
    unsigned char max_msgsize;  /* Max size of message in message queue */
    unsigned char bunlinked;    /* 1 if the msg queue has been unlinked */
#  ifdef CONFIG_HAVE_SIGNAL_H
    struct mqdes *owner;        /* Notification: Owning mqdes (NULL if none) */
    pid_t receiver_pid;         /* Notification: Receiving Task's PID */
    int signo;                  /* Notification: Signal number */
    union sigval value;         /* Notification: Signal value */
#  endif
#  ifdef CONFIG_MQUEUE_NAMELEN
    char mq_name[CONFIG_MQUEUE_NAMELEN];
#  else
    char mq_name[1];            /* Start of the queue name */
#  endif
  };
typedef struct U_mqueue_s U_mqueue_t;

#  define SIZEOF_MQ_HEADER ((int)(((U_mqueue_t*)NULL)->mq_name))

/**************************************************************************
 * Public Variables
 **************************************************************************/

/* This is a list of all opened message queues */

extern U_pthread_queue_t mqueue_open;

/**************************************************************************
 * Public Function Prototypes
 **************************************************************************/

/* Allocators */

U_mqd_internal_t *U_mqueue_allocate_descriptor(void);
void U_mqueue_free_descriptor(U_mqd_internal_t * mqdes);

U_mqueue_message_t *U_mqueue_allocate_message(unsigned int msglen);
void U_mqueue_free_message(U_mqueue_message_t * msg);

U_mqueue_t *U_mqueue_allocate_mqueue(unsigned int namelength);
void U_mqueue_deallocate_mqueue(U_mqueue_t * mqueue);

void U_mqueue_close_all(U_pthread_internal_t * thread);

/* Misc. */

U_mqueue_t *U_mqueue_find(const char *mq_name);

#endif                                 /* __MQUEUE_INTERNAL_H */
