
/**************************************************************************
 * mqueue-close.c
 * Implements U_mq_close()
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "pthread-types.h"
#include "pthread-internal.h"
#include "mqueue-internal.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

/* Close a message queue on any thread.  This is done either when
 * the current thread explicity closes a queue or when the thread
 * is cancelled and we have to clean up any message descriptors that
 * were left open.
 */

static int U_mqueue_close_descriptor(U_mqd_internal_t * mqdes)
{
  U_mqueue_t *mqueue = mqdes->mqueue;

  if (!mqueue)
    {
      return -1;
    }

  /* Check if the calling task has a notification attached to the message queue 
   * via this mqdes. */

#ifdef CONFIG_HAVE_SIGNAL_H
  if (mqueue->owner == mqdes)
    {
      mqueue->receiver_pid = INVALID_PROCESS_ID;
      mqueue->signo = 0;
      mqueue->value.sival_int = 0;
      mqueue->owner = NULL;
    }
#endif

  /* Decrement the connection count on the message queue. */

  if (mqueue->nconnections)
    mqueue->nconnections--;

  /* If it is no longer connected to any message descriptor and if the message
   * queue has already been unlinked, then we can discard the message queue. */

  if ((!mqueue->nconnections) && (mqueue->bunlinked))
    {
      /* Remove the message queue from the list of all message queues */

      U_pthread_queue_remove(&mqueue_open, mqueue);

      /* Then deallocate it (and any messages left in it) */

      U_mqueue_deallocate_mqueue(mqueue);
    }

  /* Deallocate the message descriptor */

  U_mqueue_free_descriptor(mqdes);

  return 0;
}

static int U_mqueue_close_internal(U_mqd_internal_t * mqdes,
                                   U_pthread_internal_t * thread)
{
  /* Remove the message descriptor from the thread task's list of message
   * descriptors. */

  U_pthread_queue_remove(&thread->mqdes_list, mqdes);

  return U_mqueue_close_descriptor(mqdes);
}

/**************************************************************************
 * Public Functions
 **************************************************************************/

/************************************************************
 * Function:  U_mq_close
 *
 * Description:
 * This function is used to indicate that the calling task is
 * finished with the specified message queued mqdes.  The
 * U_mq_close() deallocates any system resources allocated by the
 * system for use by this task for its message queue.
 *
 * If the calling task has attached a notification to the message
 * queue via this mqdes, this attachment will be removed and the
 * message queue is available for another process to attach a
 * notification.
 *
 * Parameters:
 *   mqdes - Message queue descriptor.
 *
 * Return Value:
 *   0 (0) if the message queue is closed successfully,
 *   otherwise, -1 (-1).
 *
 * Assumptions:
 * - The behavior of a task that is blocked on either a mq_send()
 *   or mq_receive is undefined when U_mq_close() is called.
 * - The results of using this message queue descriptor after a
 *   a successful return from U_mq_close() is undefined.
 *
 ************************************************************/

int U_mq_close(U_mqd_t mqdes)
{
  /* Verify the inputs */

  if (!mqdes)
    {
      return -1;
    }

  /* Close the message queue on the currenlty active thread */

  return U_mqueue_close_internal(mqdes, current);
}

void U_mqueue_close_all(U_pthread_internal_t * thread)
{
  U_mqd_internal_t *mqdes;

  /* Remove the message descriptor from the thread task's list of message
   * descriptors. */

  while ((mqdes = U_pthread_queue_remove_tail(&thread->mqdes_list)) != NULL)
    {
      U_mqueue_close_descriptor(mqdes);
    }
}
