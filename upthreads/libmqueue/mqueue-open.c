
/**************************************************************************
 * mqueue-open.c
 * Implements U_mq_open()
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include <stdarg.h>
#include <string.h>

#include "pthread-internal.h"
#include "mqueue-internal.h"
#include "mqueue-fcntl.h"
#include "mqueue.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

static U_mqd_internal_t *U_mqueue_create_descriptor(U_pthread_internal_t *
                                                    thread, U_mqueue_t * mqueue,
                                                    unsigned int oflags)
{
  U_mqd_internal_t *mqdes;

  /* Create a message queue descriptor */

  mqdes = U_mqueue_allocate_descriptor();
  if (mqdes)
    {
      /* Initialize the MQ descriptor */

      mqdes->mqueue = mqueue;
      mqdes->oflags = oflags;

      /* And add it to the specified thread's internal structure */

      U_pthread_queue_add_tail(&thread->mqdes_list, mqdes);
    }

  return mqdes;
}

/**************************************************************************
 * Public Functions
 **************************************************************************/

/**************************************************************************
 * Function:  U_mq_open
 *
 * Description:
 * This function establish a connection between a named
 * message queue and the calling task.  After a successful
 * call of mq_open(), the task can reference the message
 * queue using the address returned by the call.  The message
 * queue remains usable until it is closed by a successful
 * call to mq_close().
 *
 * Parameters:
 *   mq_name - Name of the queue to open
 *   oflags - open flags (see os_struct.h)
 *   ...    - Optional parameters.  When the O_CREAT flag is specified,
 *            two optional parameters are expected:
 *            1. osMode_t mode (ignored), and
 *            2. struct U_mq_attr *attr.  The mq_maxmsg attribute is used
 *               at the time that the message queue is created to determine
 *               the maximum number of messages that may be placed in the
 *               message queue.
 *
 * Return Value:
 *   A message queue descriptor or -1 (ERROR)
 *
 * Assumptions:
 *
 **************************************************************************/

U_mqd_t U_mq_open(const char *mq_name, int oflags, ...)
{
  U_mqueue_t *mqueue;
  U_mqd_internal_t *mqdes = NULL;
  int namelen;                  /* Length of MQ name */

  /* Make sure that a non-NULL name is supplied */

  if (!mq_name)
    {
      return (U_mqd_t) - 1;
    }

  namelen = strlen(mq_name);
  if (namelen <= 0)
    {
      return (U_mqd_t) - 1;
    }

  /* See if the message queue already exists */

  mqueue = U_mqueue_find(mq_name);
  if (mqueue)
    {
      /* It does.  Check if the caller wanted to create a new message queue
       * with this name. */

      if (oflags & O_CREAT && oflags & O_EXCL)
        {
          return (U_mqd_t) - 1;
        }

      /* Create a message queue descriptor */

      mqdes = U_mqueue_create_descriptor(current, mqueue, oflags);
      if (!mqdes)
        {
          return (U_mqd_t) - 1;
        }

      /* Allow a new connection to the message queue */

      mqueue->nconnections++;

    }
  else if (oflags & O_CREAT)
    {
      va_list ap;               /* Points to each un-named argument */
      unsigned int mode;        /* MQ creation mode parameter (ignored) */
      struct U_mq_attr_s *attr; /* MQ creation attributes */

      /* It doesn't exist.  Should we create one? */

      /* Allocate memory for the new message queue.  The size to allocate is
       * the size of the U_mqueue_t header plus the size of the message queue
       * name+1. */

      mqueue = U_mqueue_allocate_mqueue(namelen);
      if (!mqueue)
        {
          return (U_mqd_t) - 1;
        }

      /* Create a message queue descriptor */

      mqdes = U_mqueue_create_descriptor(current, mqueue, oflags);
      if (!mqdes)
        {
          /* Deallocate the mqueue structure. */

          U_mqueue_deallocate_mqueue(mqueue);
          return (U_mqd_t) - 1;
        }

      /* Set up to get the optional arguments needed to create a message queue. 
       */

      va_start(ap, oflags);
      mode = va_arg(ap, unsigned int);
      attr = va_arg(ap, struct U_mq_attr_s *);

      /* Initialize the new named message queue */

      if (attr)
        {
          mqueue->maxmsgs = attr->mq_maxmsg;
#ifdef CONFIG_MQUEUE_PREALLOCATED
          if (attr->mq_msgsize <= CONFIG_MQUEUE_MSGSIZE)
            mqueue->max_msgsize = attr->mq_msgsize;
          else
            mqueue->max_msgsize = CONFIG_MQUEUE_MSGSIZE;
#else
          mqueue->max_msgsize = attr->mq_msgsize;
#endif
        }
      else
        {
          mqueue->maxmsgs = CONFIG_MQUEUE_DEFAULT_MAXMSG;
#ifdef CONFIG_MQUEUE_PREALLOCATED
          mqueue->max_msgsize = CONFIG_MQUEUE_MSGSIZE;
#else
          mqueue->max_msgsize = CONFIG_MQUEUE_DEFAULT_MSGSIZE;
#endif
        }

      mqueue->nnfull_waiters = 0;
      mqueue->nnempty_waiters = 0;
      mqueue->nconnections = 1;
#ifdef CONFIG_HAVE_SIGNAL_H
      mqueue->owner = NULL;
      mqueue->receiver_pid = INVALID_PROCESS_ID;
      mqueue->signo = 0;
      mqueue->value.sival_int = 0;
#endif
      mqueue->bunlinked = 0;
      strcpy(mqueue->mq_name, mq_name);

      /* Add the new message queue to the list of message queues */

      U_pthread_queue_add_tail(&mqueue_open, mqueue);

      /* Clean-up variable argument stuff */

      va_end(ap);
    }

  if (mqdes == NULL)
    return (U_mqd_t) - 1;

  return (mqdes);
}

/**************************************************************************
 * This function finds the queue with the specified name in
 * the list of message queues.
 **************************************************************************/

U_mqueue_t *U_mqueue_find(const char *mq_name)
{
  int i;

  /* Search the list of named message queues */

  for (i = 0; i < mqueue_open.length; i++)
    {
      U_mqueue_t *mqueue = mqueue_open.list[i];

      if (!strcmp(mq_name, mqueue->mq_name))
        return mqueue;
    }

  return NULL;
}
