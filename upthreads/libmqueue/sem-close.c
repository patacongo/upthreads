
/**************************************************************************
 * sem-close.c
 * Implements U_sem_close() and related support functions
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "sem-internal.h"
#include "semaphore.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

/**************************************************************************
 * Public Functions
 **************************************************************************/

/**************************************************************************
 * Function:  U_sem_close
 *
 * Description:
 * This function is called to indicate that the calling task
 * is finished with the specified named semaphore, sem.  The
 * U_sem_close() deallocates any system resources allocated by
 * the system for this named semaphore.
 *
 * If the semaphore has not been removed with a call to
 * U_sem_unlink(), then U_sem_close() has no effect on the
 * named semaphore.  However, when the named semaphore has
 * been fully unlinked, the semaphore will vanish when the
 * last task closes it.
 *
 * Parameters:
 *  sem - semaphore descriptor
 *
 * Return Value:
 *  0, or -1 if unsuccessful.
 *
 * Assumptions:
 *   - Care must be taken to avoid risking the deletion of
 *     a semaphore that another calling task has already
 *     locked.
 *   - U_sem_close must not be called for an un-named semaphore
 *
 **************************************************************************/

int U_sem_close(U_sem_t * sem)
{
  U_nsem_t *nsem = NULL;
  int i;

  /* Verify the inputs */

  if (!sem)
    {
      return -1;
    }

  /* Search the list of named semaphores */

  for (i = 0; i < named_semaphores.length; i++)
    {
      U_nsem_t *p = named_semaphores.list[i];

      if (&p->sem == sem)
        {
          nsem = p;
        }
    }

  /* Check if we found it */

  if (!nsem)
    {
      return -1;
    }

  /* Decrement the count of sem_open connections to this semaphore */

  if (nsem->nconnections)
    nsem->nconnections--;

  /* If the semaphore is no long connected to any processes AND the semaphore
   * was previously unlinked, then deallocate it. */

  if ((!nsem->nconnections) && (nsem->unlinked))
    {
      U_pthread_queue_remove(&named_semaphores, nsem);
      U_nsem_deallocate(nsem);
    }

  return 0;
}
