
/**************************************************************************
 * Filename
 * Brief Description
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "pthread-config.h"
#include "pthread-types.h"
#include "pthread-internal.h"

/**************************************************************************
 * Public Definitions
 **************************************************************************/

#ifndef CONFIG_MEMORY_ALLOCATOR
#  undef CONFIG_SEM_PREALLOCATED
#  define CONFIG_SEM_PREALLOCATED 1
#endif

#ifdef CONFIG_SEM_PREALLOCATED
#  ifndef CONFIG_SEM_NNAMEDSEMS
#    define CONFIG_SEM_NNAMEDSEMS 32
#  endif
#  ifndef CONFIG_SEM_NAMELEN
#    define CONFIG_SEM_NAMELEN 32
#  endif
#else
#  undef CONFIG_SEM_NNAMEDSEMS
#  undef CONFIG_SEM_NAMELEN
#endif

/**************************************************************************
 * Public Types
 **************************************************************************/

/* This is the named semaphore structure */

struct U_nsem_s
  {
    unsigned short nconnections;        /* Number of connections to semaphore */
    unsigned char unlinked;     /* 1 if the semaphore has been unlinked */
    U_sem_t sem;                /* The semaphore itself */
#ifdef CONFIG_SEM_PREALLOCATED
    char name[CONFIG_SEM_NAMELEN];
#else
    char *name;                 /* Semaphore name (NULL if un-named) */
#endif
  };
typedef struct U_nsem_s U_nsem_t;

/**************************************************************************
 * Global Variables
 **************************************************************************/

/* This is a list of open named semaphores */

U_pthread_queue_t named_semaphores;

/**************************************************************************
 * Public Function Prototypes
 **************************************************************************/

/* Allocate a named semaphore structure */

U_nsem_t *U_nsem_allocate(int namelen);

/* Discard a named semaphore structure */

void U_nsem_deallocate(U_nsem_t * nsem);

/* Search the named_semaphores list to find the semaphore with
 * the matching name.
 */

U_nsem_t *U_sem_find_named(const char *name);

/* Remove a named semaphore from the named semaphore list */

void U_sem_remove_named(U_nsem_t * nsem);
