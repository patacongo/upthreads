
/**************************************************************************
 * sem-post.c
 * Implements U_sem_post()
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "sem-internal.h"
#include "pthread-internal.h"
#include "semaphore.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

/**************************************************************************
 * Public Functions
 **************************************************************************/

/**************************************************************************
 * Function:  U_sem_post
 *
 * Description:
 * When a task has finished with a semaphore, it will call
 * U_sem_post().  This function unlocks the semaphore referenced
 * by sem by performing the semaphore unlock operation on that
 * semaphore.
 *
 * If the semaphore value resulting from this operation is positive,
 * then no tasks were blocked waiting for the semaphore to
 * become unlocked; the semaphore is simply incremented.
 *
 * If the value of the semaphore resulting from this operation is
 * zero, then one of the tasks blocked waiting for the
 * semaphore shall be allowed to return successfully from its
 * call to U_sem_wait().
 *
 * Parameters:
 *   sem - Semaphore descriptor
 *
 * Return Value:
 *   0 or -1 if unsuccessful
 *
 * Assumptions:
 *
 **************************************************************************/

int U_sem_post(U_sem_t * sem)
{
  /* Make sure we were supplied with a valid semaphore. */

  if (!sem)
    {
      return -1;
    }

  if (sem->sem_count >= SEM_MAX_VALUE)
    {
      return -1;
    }

  /* Perform the semaphore unlock operation */

  sem->sem_count++;

  /* If the result of of semaphore unlock is non-positive, then there must be
   * some task waiting for the semaphore. */

  if (sem->sem_count <= 0)
    {
      int i;
      U_pthread_internal_t *waiter = NULL;

      /* Check if there are any tasks in the waiting for semaphore task list
       * that are waiting for this semaphore. This is a prioritized list so the 
       * * * first one we encounter is the one that we want. */

      for (i = 0; i < sem_waiting.length; i++)
        {
          U_pthread_internal_t *thread = sem_waiting.list[i];

          if (thread->sem == sem)
            {
              waiter = thread;
              break;
            }
        }

      if (waiter)
        {
          /* It is, let the task take the semaphore */

          waiter->sem = NULL;

          /* Remove the waiting thread from the blocked list */

          U_pthread_unblock(&sem_waiting, waiter);

          /* Then put the thread into the ready-to-run list. Note that we do
           * not give control to the unblocked waiter until the current thread
           * returns. */

          waiter->state = PTHREAD_STATE_READYTORUN;
          U_pthread_readytorun(waiter);

          /* Check if we just started a higher priority thread then ourself */

          if (waiter->priority < current->priority &&
              (current->flags & PTHREAD_FLAG_SCHEDLOCK) == 0)
            {
              /* Then let it run */

              U_pthread_schedule();
            }
        }
    }

  return 0;
}
