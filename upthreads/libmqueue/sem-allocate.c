
/**************************************************************************
 * sem-allocate.c
 * Allocates for named semaphores
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include <stdarg.h>
#include <string.h>

#include "pthread-internal.h"
#include "mqueue-fcntl.h"
#include "sem-internal.h"
#include "semaphore.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

#ifdef CONFIG_SEM_PREALLOCATED

/* This is a free list of named semaphore structures available for use */

static U_pthread_queue_t free_nsem_list;

/* Preallocated named semaphores pool */

static U_nsem_t preallocated_nsems[CONFIG_SEM_NNAMEDSEMS];

/* Number of messages not yet allocated from message pool */

static unsigned int nsems_remaining = CONFIG_SEM_NNAMEDSEMS;

#endif

/**************************************************************************
 * Private Functions
 **************************************************************************/

#ifdef CONFIG_SEM_PREALLOCATED

static U_nsem_t *U_nsem_allocate_internal(int namelen)
{
  U_nsem_t *nsem;

  if ((nsem = U_pthread_queue_remove_tail(&free_nsem_list)) == NULL)
    {
      if (nsems_remaining > 0)
        {
          --nsems_remaining;
          nsem = preallocated_nsems + nsems_remaining;
        }
    }

  if (!nsem)
    {
      return NULL;
    }

  memset(nsem, 0, sizeof(U_nsem_t));

  return nsem;
}

static void U_nsem_deallocate_internal(U_nsem_t * nsem)
{
  U_pthread_queue_add_tail(&free_nsem_list, nsem);
}

#else                                  /* ! CONFIG_SEM_PREALLOCATED */

static U_nsem_t *U_nsem_allocate_internal(int namelen)
{
  U_nsem_t *nsem = NULL;

  /* Try to allocate a new message */

  nsem = (U_nsem_t *) U_pthread_malloc(sizeof(U_nsem_t));
  if (nsem == NULL)
    {
      return NULL;
    }

  memset(nsem, 0, sizeof(U_nsem_t));

  nsem->name = U_pthread_malloc(namelen);
  if (!nsem->name)
    {
      U_pthread_free(nsem);
      return NULL;
    }

  return nsem;
}

static void U_nsem_deallocate_internal(U_nsem_t * nsem)
{
  if (nsem->name)
    {
      U_pthread_free(nsem->name);
    }

  U_pthread_free(nsem);
}

#endif                                 /* CONFIG_SEM_PREALLOCATED */

/**************************************************************************
 * Public Functions
 **************************************************************************/

/* Allocate a named semaphore structure */

U_nsem_t *U_nsem_allocate(int namelen)
{
  if (namelen <= 0)
    {
      return NULL;
    }

  return U_nsem_allocate_internal(namelen);
}

/* Discard a named semaphore structure */

void U_nsem_deallocate(U_nsem_t * nsem)
{
  if (!nsem)
    {
      return;
    }

  U_nsem_deallocate_internal(nsem);
}
