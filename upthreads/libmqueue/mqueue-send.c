
/**************************************************************************
 * mqueue-send.c
 * Implements U_mq_send()
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include <string.h>

#include "pthread-internal.h"
#include "mqueue-internal.h"
#include "mqueue-fcntl.h"
#include "mqueue.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

#define MQ_WRITEABLE(s) \
  (((s & O_ACCMODE) == O_WRONLY) || ((s & O_ACCMODE) == O_RDWR))

/* Search the message list to find the location to insert the new
 * message. Each is list is maintained in ascending priority order. */

/*
 * lower numerical value implies higher priority.
 */

void
U_mqueue_insert_message(U_pthread_queue_t * queue, U_mqueue_message_t * message)
{
  int i;

  /* Search for the location to insert this message. */

  for (i = 0; i < queue->length; i++)
    {
      U_mqueue_message_t *p = queue->list[i];

      if (message->priority < p->priority)
        {
          break;
        }
    }

  U_pthread_queue_insert(queue, message, i);
}

/**************************************************************************
 * Public Functions
 **************************************************************************/

/************************************************************
 * Function:  U_mq_send
 *
 * Description:
 * This function adds the specificied message message (msg) to
 * the message queue (mqdes).  The "msglen" parameter specifies
 * the length of the message in bytes pointed to by "msg."
 * This length must not exceed the maximum message length from
 * the mq_getattr().
 *
 * If the message queue is not full, mq_send() will in the
 * message in the message queue at the position indicated by
 * the "msgprio" argrument.  Messages with higher priority
 * will be inserted before lower priority messages.  The
 * value of "msgprio" must not exceed MQ_PRIO_MAX.
 *
 * If the specified message queue is full and O_NONBLOCK is
 * not set in the message queue, then mq_send() will block
 * until space becomes available to the queue the message.
 * 
 * If the message queue is full and osNON_BLOCK is set, the
 * message is not queued and -1 is returned.
 * 
 * Parameters:
 *   mqdes - Message queue descriptor
 *   msg - Message to send
 *   msglen - The length of the message in bytes
 *   msgprio - The priority of the message
 *
 * Return Value:
 *   None
 *
 * Assumptions/restrictions:
 *
 ************************************************************/

int U_mq_send(U_mqd_t mqdes, const void *msg, unsigned int msglen, int msgprio)
{
  U_mqd_internal_t *mqd = mqdes;
  struct U_mqueue_s *mqueue;
  U_mqueue_message_t *message;

  /* Verify the input parameters */

  if (!msg || !mqdes)
    {
      return -1;
    }

  if (!MQ_WRITEABLE(mqd->oflags))
    {
      return -1;
    }

  if (msglen <= 0 || msglen > (unsigned int)mqd->mqueue->max_msgsize)
    {
      return -1;
    }

  if (msgprio < 0 || msgprio > MQ_PRIO_MAX)
    {
      return -1;
    }

  /* Get a pointer to the message queue */

  mqueue = mqd->mqueue;

  /* (Arbitrarily) limit the number of messages in the queue to the value
   * determined when the message queue was opened. This makes us more
   * POSIX-like as well as prohibits one slow responding task from consuming
   * all available memory. */

  if (mqueue->msglist.length >= mqueue->maxmsgs)
    {
      /* Should we block until there is sufficient space in the message queue? */

      if (mqd->oflags & O_NONBLOCK)
        {
          /* No... We will return an error to the caller. */
          return -1;
        }

      /* Yes... We will not return control until the message queue is
       * available. */

      /* Loop until there are fewer than max allowable messages in the
       * receiving message queue */

      while (mqueue->msglist.length >= mqueue->maxmsgs)
        {
          /* Block until the message queue is no longer full. When we are
           * unblocked, we will try again. */

          current->state = PTHREAD_STATE_MQNFULL_WAITING;
          current->mqueue = mqueue;
          (mqueue->nnfull_waiters)++;

          /* Remove the current thrad from the ready-to-run lists */

          U_pthread_unready(current);

          /* Add it to a blocked list and return to the scheduler */

          U_pthread_block(&mqnfull_waiting, current);
          U_pthread_schedule();

          /* We are back... */
        }
    }

  /* It should be okay to add a message to the receiving mq now */

  message = U_mqueue_allocate_message(msglen);

  /* Check if we were able to get a message structure */

  if (!message)
    {
      return -1;
    }

  /* Construct the current message header info */

  message->priority = (unsigned char)msgprio;
  message->msglen = (unsigned char)msglen;

  /* Copy the message data into the message */

  memcpy(message->mail, msg, msglen);

  /* Insert the new message in the message queue */

  U_mqueue_insert_message(&mqueue->msglist, message);

  /* Check if we need to notify any tasks that are attached to the message
   * queue */

#ifdef CONFIG_HAVE_SIGNAL_H
  if (mqueue->owner)
    {
      /* Remove the message notification data from the message queue. */
      int pid = mqueue->receiver_pid;
      int signo = mqueue->signo;
      union sigval value = mqueue->value;

      /* Detach the notification */

      mqueue->receiver_pid = INVALID_PROCESS_ID;
      mqueue->signo = 0;
      mqueue->value.sival_int = 0;
      mqueue->owner = NULL;

      /* Queue the signal -- What if this returns an error? */

      signal_mqnempty(pid, signo, value);
    }
#endif

  /* Check if any tasks are waiting for the MQ not empty event. */

  if (mqueue->nnempty_waiters > 0)
    {
      int i;
      U_pthread_internal_t *waiter = NULL;

      /* Find the highest priority task that is waiting for this queue to be
       * non-empty in g_sWaitingForMQNotEmptyList list. */

      for (i = 0; i < mqnempty_waiting.length; i++)
        {
          U_pthread_internal_t *thread = mqnempty_waiting.list[i];

          if (thread->mqueue == mqueue)
            {
              waiter = thread;
              break;
            }
        }

      /* If one was found, unblock it.  It is an error if one was not found
       * (since nnot_empty_waiters > 0), but we don't have a good way to report 
       * * * that. */

      if (waiter)
        {
          /* No longer waiting */

          waiter->mqueue = NULL;
          (mqueue->nnempty_waiters)--;

          /* Remove the waiting thread from the blocked list */

          U_pthread_unblock(&mqnempty_waiting, waiter);

          /* Then put the thread into the ready-to-run list. Note that we do
           * not give control to the unblocked waiter until the current thread
           * returns. */

          waiter->state = PTHREAD_STATE_READYTORUN;
          U_pthread_readytorun(waiter);

          /* Check if we just started a higher priority thread then ourself */

          if (waiter->priority < current->priority
              && (current->flags & PTHREAD_FLAG_SCHEDLOCK) == 0)
            {
              /* Then let it run */

              U_pthread_schedule();
            }
        }
    }

  return 0;
}
