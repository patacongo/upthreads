
/**************************************************************************
 * mqueue-unlink.c
 * Implements U_mq_unlink()
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "mqueue-internal.h"
#include "mqueue.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

/**************************************************************************
 * Public Functions
 **************************************************************************/

/************************************************************
 * Function:  U_mq_unlink
 *
 * Description:
 * This function removes the message queue named by "mq_name."
 * If one or more tasks have the message queue open when
 * mq_unlink() is called, removal of the message queue is
 * postponed until all references to the message queue have
 * been closed.
 * 
 * Parameters:
 *   mq_name - Name of the message queue
 *
 * Return Value:
 *   None
 *
 * Assumptions:
 *
 ************************************************************/

int U_mq_unlink(const char *mq_name)
{
  U_mqueue_t *mqueue;

  /* Verify the input values */

  if (!mq_name)
    {
      return -1;
    }

  /* Find the named message queue */

  mqueue = U_mqueue_find(mq_name);
  if (!mqueue)
    {
      return -1;
    }

  /* If it is no longer connected, then we can just discard the message queue
   * now. */

  if (!mqueue->nconnections)
    {
      /* Remove the message queue from the list of all message queues */

      U_pthread_queue_remove(&mqueue_open, mqueue);

      /* Then deallocate it (and any messages left in it) */

      U_mqueue_deallocate_mqueue(mqueue);
    }
  else
    {

      /* If the message queue is still connected to a message descriptor, then
       * mark it for deletion when the last message descriptor is closed */

      mqueue->bunlinked = 1;
    }

  return 0;
}
