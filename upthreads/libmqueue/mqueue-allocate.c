
/**************************************************************************
 * mqueue-allocate.c
 * Allocates for messages and descriptors
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include <string.h>
#include "pthread-config.h"
#include "pthread-types.h"
#include "mqueue-internal.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/* This is a list of all opened message queues */

U_pthread_queue_t mqueue_open;

/**************************************************************************
 * Private Variables
 **************************************************************************/

#ifdef CONFIG_MQUEUE_PREALLOCATED

/* The free_mqd_list is a list of message descriptors available to the
 * for use. The number of messages in the pool is a constant.
 */

static U_pthread_queue_t free_mqd_list;

/* Preallocated message queue descriptors pool. */

static U_mqd_internal_t preallocated_mqds[CONFIG_MQUEUE_NDESCRIPTORS];

/* Number of message queue descriptors not yet alloated from the mqd pool. */

static unsigned int mqds_remaining = CONFIG_MQUEUE_NDESCRIPTORS;

/* The free_message_list is a list of message descriptors available to the
 * for use. The number of messages in the pool is a constant.
 */

static U_pthread_queue_t free_mqueue_list;

/* Preallocated message queues pool. */

static U_mqueue_t preallocated_mqueues[CONFIG_MQUEUE_NQUEUES];

static unsigned int mqueues_remaining = CONFIG_MQUEUE_NQUEUES;

/* The free_message_list is a list of message descriptors available to the
 * for use. The number of messages in the pool is a constant.
 */

static U_pthread_queue_t free_message_list;

/* Preallocated messages pool. */

static U_mqueue_message_t preallocated_messages[CONFIG_MQUEUE_NMESSAGES];

/* Number of messages not yet allocated from message pool */

static unsigned int messages_remaining = CONFIG_MQUEUE_NMESSAGES;

#endif

/**************************************************************************
 * Private Functions
 **************************************************************************/

#ifdef CONFIG_MQUEUE_PREALLOCATED

/*
 *message queue descriptor internal functions
 */

static U_mqd_internal_t *U_mqueue_allocate_descriptor_internal(void)
{
  U_mqd_internal_t *mqdes;

  if ((mqdes = U_pthread_queue_remove_tail(&free_mqd_list)) != NULL)
    {
      return mqdes;
    }

  if (mqds_remaining > 0)
    {
      --mqds_remaining;
      return preallocated_mqds + mqds_remaining;
    }

  return NULL;
}

static void U_mqueue_free_descriptor_internal(U_mqd_internal_t * mqdes)
{
  U_pthread_queue_add_tail(&free_mqd_list, mqdes);
}

/*
 * message queue allocation internal functions
 */

static U_mqueue_t *U_mqueue_allocate_mqueue_internal(unsigned int namelength)
{
  U_mqueue_t *mqueue;

  if ((mqueue = U_pthread_queue_remove_tail(&free_mqueue_list)) != NULL)
    {
      return mqueue;
    }

  if (mqueues_remaining > 0)
    {
      --mqueues_remaining;
      return preallocated_mqueues + mqueues_remaining;
    }

  return NULL;
}

static void U_mqueue_free_mqueue_internal(U_mqueue_t * mqueue)
{
  if (mqueue)
    {
      U_pthread_queue_add_tail(&free_mqueue_list, mqueue);
    }
}

/*
 * message allocation internal functions
 */

static U_mqueue_message_t *U_mqueue_allocate_message_internal(unsigned int
                                                              msglen)
{
  U_mqueue_message_t *message;

  if ((message = U_pthread_queue_remove_tail(&free_message_list)) != NULL)
    {
      return message;
    }

  if (messages_remaining > 0)
    {
      --messages_remaining;
      return preallocated_messages + messages_remaining;
    }

  return NULL;
}

static void U_mqueue_free_message_internal(U_mqueue_message_t * message)
{
  U_pthread_queue_add_tail(&free_message_list, message);
}

#else                                  /* ! CONFIG_MQUEUE_PREALLOCATED */

/*
 *message queue descriptor internal functions
 */

static U_mqd_internal_t *U_mqueue_allocate_descriptor_internal(void)
{
  return U_pthread_malloc(sizeof(U_mqd_internal_t));
}

static void U_mqueue_free_descriptor_internal(U_mqd_internal_t * mqdes)
{
  U_pthread_free(mqdes);
}

/*
 * message queue allocation internal functions
 */

static U_mqueue_t *U_mqueue_allocate_mqueue_internal(unsigned int namelength)
{
  int alloc_size = SIZEOF_MQ_HEADER + namelength + 1;

  U_mqueue_t *mqueue = (U_mqueue_t *) U_pthread_malloc(alloc_size);

  return mqueue;
}

static void U_mqueue_free_mqueue_internal(U_mqueue_t * mqueue)
{
  if (mqueue)
    {
      U_pthread_free(mqueue);
    }
}

/*
 * message allocation internal functions
 */

static U_mqueue_message_t *U_mqueue_allocate_message_internal(unsigned int
                                                              msglen)
{
  /* Try to allocate a new message */

  U_mqueue_message_t *message = U_pthread_malloc(sizeof(U_mqueue_message_t));

  if (!message)
    {
      return NULL;
    }

  memset(message, 0, sizeof(U_mqueue_message_t));
#  ifndef CONFIG_MQUEUE_MSGSIZE
  if (msglen > 0)
    {
      message->mail = U_pthread_malloc(msglen);
      if (!message->mail)
        {
          U_pthread_free(message);
          return NULL;
        }
    }
#  endif

  return message;
}

static void U_mqueue_free_message_internal(U_mqueue_message_t * message)
{
#  ifndef CONFIG_MQUEUE_MSGSIZE
  if (message->mail)
    {
      /* Free the message data */

      U_pthread_free(message->mail);
    }
#  endif
  /* Free the message */

  U_pthread_free(message);
}

#endif                                 /* CONFIG_MQUEUE_PREALLOCATED */

        /* Deallocate any stranded messages in the message queue. */

static void U_mqueue_deallocate_message_list(U_mqueue_t * mqueue)
{
  U_mqueue_message_t *message;

  while ((message = U_pthread_queue_remove_tail(&mqueue->msglist)) != NULL)
    {
      /* Deallocate the message structure. */

      U_mqueue_free_message_internal(message);
    }
}

/**************************************************************************
 * Public Functions
 **************************************************************************/

/* Allocate a message queue desriptor */

U_mqd_internal_t *U_mqueue_allocate_descriptor(void)
{
  return U_mqueue_allocate_descriptor_internal();
}

/* Free a message queue descriptor */

void U_mqueue_free_descriptor(U_mqd_internal_t * mqdes)
{
  if (mqdes)
    {
      U_mqueue_free_descriptor_internal(mqdes);
    }
}

/* Allocate a message */

U_mqueue_message_t *U_mqueue_allocate_message(unsigned int msglen)
{
  return U_mqueue_allocate_message_internal(msglen);
}

/* Free a message */

void U_mqueue_free_message(U_mqueue_message_t * message)
{
  if (message)
    {
      U_mqueue_free_message_internal(message);
    }
}

/* Allocate a message queue structure */

U_mqueue_t *U_mqueue_allocate_mqueue(unsigned int namelength)
{
  int alloc_size = SIZEOF_MQ_HEADER + namelength + 1;
  U_mqueue_t *mqueue = U_mqueue_allocate_mqueue_internal(namelength);

  if (mqueue)
    {
      memset(mqueue, 0, alloc_size);
    }

  return mqueue;
}

/* This function deallocates an initialized message queue structure.  First,
 * it deallocates all of the queued messages in the message Q.  It is assumed
 * that this message is fully unlinked and closed so that no thread will
 * attempt access it while it is being deleted.
 */

void U_mqueue_deallocate_mqueue(U_mqueue_t * mqueue)
{
  U_mqueue_deallocate_message_list(mqueue);

  /* Then deallocate the message queue itself */

  U_mqueue_free_mqueue_internal(mqueue);
}
