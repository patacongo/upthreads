
/**************************************************************************
 * sem-open.c
 * Implements U_sem_open() and related support functions.
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include <stdarg.h>
#include <string.h>

#include "pthread-internal.h"
#include "mqueue-fcntl.h"
#include "sem-internal.h"
#include "semaphore.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/* This is a list of open named semaphores */

U_pthread_queue_t named_semaphores;

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

/**************************************************************************
 * Public Functions
 **************************************************************************/

/**************************************************************************
 * Function:  U_sem_open
 *
 * Description:
 * This function establishes a connection between named
 * semaphores and a task.  Following a call to U_sem_open()
 * with the semaphore name, the task may reference the
 * semaphore associated with name using the address
 * returned by this call.  The semaphore may be used in
 * subsequent calls to U_sem_wait(), U_sem_trywait(), and
 * U_sem_post().  The semaphore remains usable until the
 * semaphore is closed by a successful call to U_sem_close().
 *
 * If a task makes multiple calls to U_sem_open() with the
 * same name, then the same semaphore address is returned
 * (provided there have been no calls to U_sem_unlink()).
 *
 * Parameters:
 *   name  - Semaphore name
 *   oflag - Semaphore creation options.  This may either
 *     or both of the following bit settings.
 *     oflag = 0:  Connect to the semaphore only if it
 *        already exists.
 *     oflag = O_CREAT:  Connect to the semaphore if it
 *        exists, otherwise create the semaphore.
 *     oflag = O_CREAT|O_EXCL:  Create a new semaphore
 *        unless one of this name already exists.
 *   ... - Optional parameters.  When the O_CREAT flag is specified,
 *        two optional parameters are expected:
 *        1. unsigned int mode (ignored), and
 *        2. unsigned int value.  This initial value of the semaphore.
 *           valid initial values of the semaphore must be less than
 *           or equal to SEM_MAX_VALUE.
 *
 * Return Value:
 *   A pointer to U_sem_t or -1 if unsuccessful.
 *
 * Assumptions:
 *
 **************************************************************************/

U_sem_t *U_sem_open(const char *name, int oflag, ...)
{
  int namelen;
  U_nsem_t *nsem;
  U_sem_t *sem = (U_sem_t *) - 1;
  va_list ap;                   /* Points to each un-named argument */
  unsigned int mode;            /* Creation mode parameter (ignored) */
  unsigned int value;           /* Semaphore value parameter */

  /* Make sure that a non-NULL name is supplied */

  if (!name)
    {
      return sem;
    }

  namelen = strlen(name);
  if (namelen <= 0)
    {
      return sem;
    }
  /* See if the semaphore already exists */

  nsem = U_sem_find_named(name);
  if (nsem)
    {
      /* It does.  Check if the caller wanted to created a new semahore with
       * this name. */

      if ((!(oflag & O_CREAT)) || (!(oflag & O_EXCL)))
        {
          /* Allow a new connection to the semaphore */

          nsem->nconnections++;
          sem = &nsem->sem;
        }
    }
  else if ((oflag & O_CREAT) != 0)
    {

      /* It doesn't exist.  Should we create one? */

      /* Set up to get the optional arguments needed to create a message queue. 
       */

      va_start(ap, oflag);
      mode = va_arg(ap, unsigned int);
      value = va_arg(ap, unsigned int);

      /* Verify that a legal initial value was selected. */

      if (value <= SEM_MAX_VALUE)
        {
          /* Allocate memory for the new semaphore */

          nsem = U_nsem_allocate(namelen);
          if (nsem)
            {
              /* Initialize the named semaphore */

              sem = &nsem->sem;
              U_sem_init(sem, 0, value);

              nsem->nconnections = 1;
              nsem->unlinked = 0;
#ifdef CONFIG_SEM_NAMELEN
              strncpy(nsem->name, name, CONFIG_SEM_NAMELEN);
#else
              strcpy(nsem->name, name);
#endif

              /* Add the new semaphore to the list of named semaphores */

              U_pthread_queue_add_tail(&named_semaphores, nsem);
            }

          /* Clean-up variable argument stuff */

          va_end(ap);
        }
    }

  return sem;
}

/* Search the named_semaphores list to find the semaphore with
 * the matching name.
 */

U_nsem_t *U_sem_find_named(const char *name)
{
  U_nsem_t *nsem = NULL;
  int i;

  /* Search the list of named semaphores */

  for (i = 0; i < named_semaphores.length; i++)
    {
      nsem = named_semaphores.list[i];
#ifdef CONFIG_SEM_NAMELEN
      if (!strncmp(name, nsem->name, CONFIG_SEM_NAMELEN))
        break;
#else
      if (!strcmp(name, nsem->name))
        break;
#endif
    }

  return nsem;
}
