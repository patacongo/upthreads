
/**************************************************************************
 * mqueue-attr.c
 * Implements U_mq_getattr() and U_mq_settr()
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "mqueue-internal.h"
#include "mqueue-fcntl.h"
#include "mqueue.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

static void U_mqueue_getattr_internal(U_mqd_internal_t * mqdes,
                                      struct U_mq_attr_s *attr)
{
  /* Return the attributes */

  attr->mq_maxmsg = mqdes->mqueue->maxmsgs;
  attr->mq_msgsize = mqdes->mqueue->max_msgsize;
  attr->mq_flags = mqdes->oflags;
  attr->mq_curmsgs = mqdes->mqueue->msglist.length;
}

static void U_mqueue_setattr_internal(U_mqd_internal_t * mqdes,
                                      const struct U_mq_attr_s *attr,
                                      struct U_mq_attr_s *old_attr)
{
  /* Return the attributes if so requested */

  if (old_attr)
    {
      U_mqueue_getattr_internal(mqdes, old_attr);
    }

  /* Set the new value of the O_NONBLOCK flag. */

  mqdes->oflags = (unsigned int)((attr->mq_flags & O_NONBLOCK) |
                                 (mqdes->oflags & (~O_NONBLOCK)));
}

/**************************************************************************
 * Public Functions
 **************************************************************************/

/************************************************************
 * Function:  U_mq_setattr
 *
 * Description:
 * This function sets the attributes associated with the
 * specified message queue "mqdes."  Only the "O_NONBLOCK"
 * bit of the "mq_flags" can be changed.
 *
 * If "old_attr" is non-null, mq_setattr() will store the
 * previous message queue attributes at that location (just
 * as would have been returned by mq_getattr()).
 *
 * Parameters:
 *   mqdes - Message queue descriptor
 *   attr - New attributes
 *   old_attr - Old attributes
 *
 * Return Value:
 *   0 (0) if attributes are set successfully, otherwise
 *   -1 (-1).
 *
 * Assumptions:
 *
 ************************************************************/

int U_mq_setattr(U_mqd_t mqdes, const struct U_mq_attr_s *attr,
                 struct U_mq_attr_s *old_attr)
{
  if (!mqdes || !attr)
    {
      return -1;
    }

  U_mqueue_setattr_internal(mqdes, attr, old_attr);

  return 0;
}

/************************************************************
 * Function:  U_mq_getattr
 *
 * Description:
 * This functions gets status information and attributes
 * associated with the specified message queue.
 *
 * Parameters:
 *   mqdes - Message queue descriptor
 *   attr - Buffer in which to return attributes (see
 *      os_struct.h for description.
 *
 * Return Value:
 *   0 (0) if attributes provided, -1 (-1) otherwise.
 *
 * Assumptions:
 *
 ************************************************************/

int U_mq_getattr(U_mqd_t mqdes, struct U_mq_attr_s *attr)
{
  if (!mqdes || !attr)
    {
      return -1;
    }

  U_mqueue_getattr_internal(mqdes, attr);

  return 0;
}
