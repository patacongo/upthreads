
/**************************************************************************
 * sem-unlink.c
 * Implements U_sem_unlink()
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "sem-internal.h"
#include "semaphore.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

/**************************************************************************
 * Public Functions
 **************************************************************************/

/**************************************************************************
 * Function:  U_sem_unlink
 *
 * Description:
 * This function removes the semaphore named by the input
 * parameter "name."  If the semaphore named by "name" is
 * currently referenced by other processes, the U_sem_unlink()
 * will have no effect on the state of the semaphore.  If
 * one or more processes have the semaphore open when U_sem_unlink()
 * is called, destruction of the semaphore will be postponed
 * until all references to the semaphore have been destroyed
 * by calls of U_sem_close().
 *
 * Parameters:
 *   name - Semaphore name
 *
 * Return Value:
 *  0, or -1 if unsuccessful.
 *
 * Assumptions:
 *
 **************************************************************************/

int U_sem_unlink(const char *name)
{
  U_nsem_t *nsem;

  /* Verify the input values */

  if (!name)
    {
      return -1;
    }

  /* Find the named semaphore */

  nsem = U_sem_find_named(name);

  /* Check if the semaphore was found */

  if (!nsem)
    {
      return -1;
    }

  /* If the named semaphore was found and if there are no connects to it, then
   * deallocate it */
  if (!nsem->nconnections)
    {
      U_pthread_queue_remove(&named_semaphores, nsem);
      U_nsem_deallocate(nsem);
    }
  else
    {

      /* If one or more process still has the semaphore open, then just mark it 
       * as unlinked.  The unlinked semaphore will be deleted when the final
       * process closes the semaphore. */

      nsem->unlinked = 1;
    }

  return 0;
}
