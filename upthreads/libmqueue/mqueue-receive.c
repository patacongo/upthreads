
/**************************************************************************
 * mqueue-receive.c
 * Implements U_mq_receive
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include <string.h>

#include "pthread-internal.h"
#include "mqueue-fcntl.h"
#include "mqueue-internal.h"
#include "mqueue.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

#define MQ_READABLE(s) \
  (((s & O_ACCMODE) == O_RDONLY) || ((s & O_ACCMODE) == O_RDWR))

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

/**************************************************************************
 * Public Functions
 **************************************************************************/

/************************************************************
 * Function:  U_mq_receive
 *
 * Description:
 * This function receives the oldest of the highest priority
 * messages from the message queue specified by "mqdes."  If
 * the size of the buffer in bytes (msglen) is less than the
 * the "mq_msgsize" attribute of the message queue, mq_receive
 * will return an error.  Otherwise, the select message is
 * removed from the queue and copied to "msg."
 *
 * If the message queue is empty and O_NONBLOCK was not set,
 * mq_receive() will block until a message is added to the
 * message queue.  If more than one task is waiting to
 * receive a message, only the task with the highest priority
 * that has waited the longest will be unblocked.
 *
 * If the queue is empty and O_NONBLOCK is set, -1 will
 * be returned.
 *
 * Parameters:
 *   mqdes - Message Queue Descriptor
 *   msg - Buffer to receive the message
 *   msglen - Size of the buffer in bytes
 *   msgprio - If not NULL, the location to store message
 *      priority.
 *
 * Return Value:
 *   Length of the selected message in bytes, otherwise -1
 *   (-1).
 *
 * Assumptions:
 *
 ************************************************************/

int U_mq_receive(U_mqd_t mqdes, void *msg, unsigned int msglen, int *msgprio)
{
  U_mqd_internal_t *mqd = mqdes;
  struct U_mqueue_s *mqueue;
  U_mqueue_message_t *message = NULL;
  unsigned char rcv_msglen;
  int retval = -1;

  /* Verify the input parameters */

  if (!msg || !mqdes)
    {
      return -1;
    }

  if (!MQ_READABLE(mqd->oflags))
    {
      return -1;
    }

  if (msglen < mqd->mqueue->max_msgsize)
    {
      return -1;
    }

  /* Get a pointer to the message queue */

  mqueue = mqd->mqueue;

  /* Several operations must be performed below: We must determine if a message 
   * is pending and, if not, wait for the message. */

  /* Loop until we get a message (unless NON_BLOCKING */

  /* Check if the list is empty */

  while ((message = U_pthread_queue_remove_head(&mqueue->msglist)) == NULL)
    {
      /* The messsage queue is empty.  Should we block until the queue is
       * non-empty? */

      if (mqd->oflags & O_NONBLOCK)
        {
          return -1;
        }

      /* Block until the message queue is no longer empty. When we are
       * unblocked, we will try again. */

      current->state = PTHREAD_STATE_MQNEMPTY_WAITING;
      current->mqueue = mqueue;
      mqueue->nnempty_waiters++;

      /* Remove the current thrad from the ready-to-run lists */

      U_pthread_unready(current);

      /* Add it to a blocked list and return to the scheduler */

      U_pthread_block(&mqnempty_waiting, current);
      U_pthread_schedule();

      /* We are back... */
    }

  /* Get the length of the message (also the return value) */

  retval = rcv_msglen = message->msglen;

  /* Copy the message into the caller's buffer */

  memcpy(msg, message->mail, rcv_msglen);

  /* Copy the message priority as well (if a buffer is provided) */

  if (msgprio)
    {
      *msgprio = message->priority;
    }

  /* We are done with the message.  Deallocate it now. */

  U_mqueue_free_message(message);

  /* Check if any tasks are waiting for the MQ not full event. */

  if (mqueue->nnfull_waiters > 0)
    {
      int i;
      U_pthread_internal_t *waiter = NULL;

      /* Find the highest priority task that is waiting for this queue to be
       * not-full in g_sWaitingForMQNotFullList list. */

      for (i = 0; i < mqnempty_waiting.length; i++)
        {
          U_pthread_internal_t *thread = mqnempty_waiting.list[i];

          if (thread->mqueue == mqueue)
            {
              waiter = thread;
              break;
            }
        }

      /* If one was found, unblock it.  It is an error if one was not found
       * (since nnot_empty_waiters > 0), but we don't have a good way to report 
       * * * that. */

      if (waiter)
        {
          /* No longer waiting */

          waiter->mqueue = NULL;
          mqueue->nnfull_waiters--;

          /* Remove the waiting thread from the blocked list */

          U_pthread_unblock(&mqnfull_waiting, waiter);

          /* Then put the thread into the ready-to-run list. Note that we do
           * not give control to the unblocked waiter until the current thread
           * returns. */

          waiter->state = PTHREAD_STATE_READYTORUN;
          U_pthread_readytorun(waiter);

          /* Check if we just started a higher priority thread then ourself */

          if (waiter->priority < current->priority
              && (current->flags & PTHREAD_FLAG_SCHEDLOCK) == 0)
            {
              /* Then let it run */

              U_pthread_schedule();
            }
        }
    }

  return retval;
}
