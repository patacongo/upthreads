
/**************************************************************************
 * sem-wait.c
 * Implements U_sem_wait() and U_sem_trywait().
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "sem-internal.h"
#include "pthread-internal.h"
#include "semaphore.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

/**************************************************************************
 * Public Functions
 **************************************************************************/

/**************************************************************************
 * Function:  U_sem_wait
 *
 * Description:
 * This function attempts to lock the semaphore referenced by
 * sem.  If the semaphore value is (<=) zero, then the calling
 * task will not return until it successfully acquires the lock.
 *
 * Parameters:
 *   sem - Semaphore descriptor.
 *
 * Return Value:
 *   0, or -1 if unsuccessful
 *
 * Assumptions:
 *
 **************************************************************************/

int U_sem_wait(U_sem_t * sem)
{
  if (!sem)
    {
      return -1;
    }

  /* Check if the lock is available */

  if (sem->sem_count > 0)
    {
      /* It is, let the task take the semaphore. */

      sem->sem_count--;
      current->sem = NULL;
      return 0;
    }

  /* The semaphore is NOT available, We will have to block the current thread
   * of execution. */

  /* Handle the POSIX semaphore */

  sem->sem_count--;

  /* Mark the thread waiting for a semaphore */

  current->state = PTHREAD_STATE_SEM_WAITING;
  current->sem = sem;

  /* Remove the current thread from the ready-to-run lists */

  U_pthread_unready(current);

  /* Add it to a blocked list and return to the scheduler */

  U_pthread_block(&sem_waiting, current);
  U_pthread_schedule();

  /* When we resume at this point, the semaphore has been assigned to this
   * thread of execution. */

  current->sem = NULL;

  return 0;
}

/**************************************************************************
 * Function:  U_sem_trywait
 *
 * Description:
 * This function locks the specified semaphore only if the
 * semaphore is currently not locked.  Otherwise, it locks
 * the semaphore.  In either case, the call returns without
 * blocking.
 *
 * Parameters:
 *   sem - the semaphore descriptor
 *
 * Return Value:
 *   0 or -1 if unsuccessful (probably because the semaphore is not
 *   available).
 *
 * Assumptions:
 *
 **************************************************************************/

int U_sem_trywait(U_sem_t * sem)
{
  if (!sem)
    {
      return -1;
    }

  /* If the semaphore is available, give it to the requesting task */

  if (sem->sem_count <= 0)
    {
      return -1;
    }

  /* It is, let the task take the semaphore */

  sem->sem_count--;
  current->sem = NULL;

  return 0;
}
