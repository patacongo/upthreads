
/**************************************************************************
 * pthread-queue.c
 * Implements U_pthread_queue_t related operations
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include <stdlib.h>
#include <string.h>
#include "pthread-internal.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

#ifdef CONFIG_MEMORY_ALLOCATOR
static void U_pthread_queue_expand(U_pthread_queue_t * queue, int size)
{
  void **plist;

  /* expand the queue size */

  plist =
    U_pthread_realloc(queue->list, sizeof(void *) * (queue->length + size));

  if (!plist)
    {
      err("pthread_queue_expand(): failed to grow the queue\n");
      return;
    }

  queue->list = plist;
  queue->length++;
}
#else
static void U_pthread_queue_expand(U_pthread_queue_t * queue, int size)
{
  if (queue->length + size > CONFIG_QUEUE_SIZE)
    {
      err("pthread_queue_expand(): failed to grow the queue\n");
      return;
    }

  queue->length++;
}
#endif

static int U_pthread_queue_find(U_pthread_queue_t * queue, void *item)
{
  int i;

  for (i = 0; i < queue->length; i++)
    {
      if (queue->list[i] == item)
        {
          return i;
        }
    }

  return -1;
}

/**************************************************************************
 * Public Functions
 **************************************************************************/

void U_pthread_queue_add_tail(U_pthread_queue_t * queue, void *item)
{
  U_pthread_queue_expand(queue, 1);

  /* Add the new item to the tail of the queue */

  queue->list[queue->length - 1] = item;
}

void U_pthread_queue_insert(U_pthread_queue_t * queue, void *item, int index)
{
  U_pthread_queue_expand(queue, 1);

  if (index < queue->length - 1)
    {
      /* move all threads after inserted thread 1 chunk behind */

      memmove(&queue->list[index + 1], &queue->list[index],
              sizeof(void *) * (queue->length - index));
    }

  /* insert the thread */

  queue->list[index] = item;
}

void *U_pthread_queue_remove_index(U_pthread_queue_t * queue, int index)
{
  void *item;

  if (index < 0 || index >= queue->length)
    {
      err("pthread_queue_remove_index(): item [%d] not in queue %x\n", index,
          queue);
      return NULL;
    }

  item = queue->list[index];

  /* if item is not at the tail of queue */

  if (index < queue->length - 1)
    {
      /* move all threads after removed item 1 chunk ahead */

      memmove(&queue->list[index], &queue->list[index + 1],
              sizeof(void *) * (queue->length - index - 1));
    }

  queue->length--;

  return item;
}

void *U_pthread_queue_remove(U_pthread_queue_t * queue, void *item)
{
  int i;

  if ((i = U_pthread_queue_find(queue, item)) < 0)
    {
      err("pthread_queue_remove(): item %x not in queue %x\n", item, queue);
      return NULL;
    }

  return U_pthread_queue_remove_index(queue, i);
}

void *U_pthread_queue_remove_head(U_pthread_queue_t * queue)
{
  if (!queue)
    {
      return NULL;
    }

  if (queue->length <= 0)
    {
      return NULL;
    }

  return U_pthread_queue_remove_index(queue, 0);
}

void *U_pthread_queue_remove_tail(U_pthread_queue_t * queue)
{
  if (!queue)
    {
      return NULL;
    }

  if (queue->length <= 0)
    {
      return NULL;
    }

  return U_pthread_queue_remove_index(queue, queue->length - 1);
}
