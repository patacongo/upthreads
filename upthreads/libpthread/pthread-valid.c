
/**************************************************************************
 * pthread-valid.c
 * Implements U_pthread_valid() and U_pthread_active() which verify that
 * a user pthread handle refers to a valid internal pthread structure.
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "pthread-internal.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

/**************************************************************************
 * Public Functions
 **************************************************************************/

/* Return 1 if the handle points to a valid U_pthread_internal_t, and
 * 0 otherwise (Not 100% foolproof but pretty damn good)
 */

int U_pthread_valid(const U_pthread_internal_t * internal_thread)
{
  /* Sanity check */

  if (!internal_thread)
    {
      return 0;
    }

  /* For the user thread handle to be valid, it must be non-NULL, point to a
   * initialized thread, and verification_key must match the one that the user
   * originally obtained with the thread (in case the thread memory is reused. */

  if (internal_thread &&
      internal_thread->state != PTHREAD_STATE_UNINITIALIZED &&
      internal_thread->verification_key == PTHREAD_VERIFICATION_KEY)
    {
      return 1;
    }

  return 0;
}

#if 0

/* Return 1 if the handle points to an active U_pthread_internal_t, and
 * 0 otherwise (Not 100% foolproof but pretty damn good)
 */

int U_pthread_active(const struct pthread_s *thread)
{
  int retval = 0;

  /* The conditions are the same as for a valid thread, but the thread must not 
   * be defunct as well. */

  if (U_pthread_valid(thread) == 1)
    {
      /* Convert the void* reference to something a little more friendly */

      U_pthread_internal_t *internal_thread =
        (U_pthread_internal_t *) thread->internal_thread;

      /* Make sure that the state is not defunct */

      if (internal_thread->state != PTHREAD_STATE_DEFUNCT &&
          internal_thread->state != PTHREAD_STATE_DETACHED)
        {
          retval = 1;
        }
    }

  return retval;
}
#endif
