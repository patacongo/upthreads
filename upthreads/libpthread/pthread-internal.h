
/**************************************************************************
 * pthread-internal.h
 * Private definitions used only with in the pthread library
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

#ifndef __PTHREAD_INTERNAL_H
#  define __PTHREAD_INTERNAL_H

/**************************************************************************
 * Included Files
 **************************************************************************/

#  include "pthread-config.h"
#  include "pthread.h"
#  include "pthread-errno.h"

/**************************************************************************
 * Public Defintions
 **************************************************************************/

/* pthread priority is set using pthread_attr_setparam().  The
 * system sched_get_priority_min and sched_get_priority_max should
 * not be used.  Rather, the following, non-standard defintion
 * should be used:
 */

/* This is the number of priority level */

#  define PTHREAD_PRIORITY_LEVELS	(CONFIG_NPRIORITY_LEVELS)

/* If no priority attribute is specified, this is the default priority
 * that will be used (basically something near the middle):
 */

#  define PTHREAD_PRIORITY_DEFAULT ((PTHREAD_PRIORITY_LEVELS >> 1) + PTHREAD_PRIORITY_MIN)

/*
 * This is used to convert sched_priority (higher numerical value
 * implies higher priority) to our internal priority (lower numerical
 * value implies higher priority).
 */

#  define PTHREAD_PRIORITY_INTERNAL(x)	((PTHREAD_PRIORITY_MAX) - (x) + (PTHREAD_PRIORITY_MIN))

/* Pthread flags */

#  define PTHREAD_FLAG_DETACHED      0x00000001
#  define PTHREAD_FLAG_NONCANCELABLE 0x00000002
#  define PTHREAD_FLAG_SCHEDLOCK     0x00000004

/* Select how many slots (keys) will be set aside for thread specific
 * data.  If CONFIG_NKEYS is not defined, then the system default will
 * be used.  You can eliminate all thread specific data support by
 * setting CONFIG_NKEYS to zero.
 */

#  ifndef CONFIG_NKEYS
#    define CONFIG_NKEYS PTHREAD_DEFAULT_KEYS_MAX
#  endif

/* The size of the scheduler stack */

#  ifndef CONFIG_SCHEDULER_STACKSIZE
#    define CONFIG_SCHEDULER_STACKSIZE 1024
#  endif

/* Error messages will be output on stdout using printf if
 * CONFIG_DEFAULT_STDOUT is defined.  If CONFIG_DEFAULT_STDOUT
 * is defined, and CONFIG_DEFAULT_LOGGER is defined, then error
 * messages will be presented on CONFIG_DEFAULT_LOGGER.  If
 * neithe are defined then no output will be produced.
 */

#  ifdef CONFIG_DEFAULT_STDOUT
#    undef CONFIG_DEFAULT_LOGGER
#    define CONFIG_DEFAULT_LOGGER printf
#  endif

#  ifdef CONFIG_DEFAULT_LOGGER
#    ifdef CONFIG_PTHREAD_DEBUG
#      define dbg(fmt,arg...) U_pthread_log(fmt, ##arg)
#      define dbg1(fmt,arg...) U_pthread_log(fmt, ##arg)
#      if CONFIG_PTHREAD_DEBUG > 1
#        define dbg2(fmt,arg...) U_pthread_log(fmt, ##arg)
#        if CONFIG_PTHREAD_DEBUG > 2
#          define dbg3(fmt,arg...) U_pthread_log(fmt, ##arg)
#        else
#          define dbg3(fmt...)
#        endif
#      else
#        define dbg2(fmt...)
#        define dbg3(fmt...)
#      endif
#    else
#      define dbg(fmt...)
#      define dbg1(fmt...)
#      define dbg2(fmt...)
#      define dbg3(fmt...)
#    endif
#    define err(fmt,arg...) U_pthread_log(fmt, ##arg)
#  else
#    define dbg(fmt...)
#    define dbg1(fmt...)
#    define dbg2(fmt...)
#    define dbg3(fmt...)
#    define err(fmt...)
#  endif

/* Array indices of jmpbuf */

#  if defined(CONFIG_ARCH_X86)

  /* Storage order: %ebx, $esi, %edi, %ebp, sp, and return PC */

#    define JB_EBX (0)
#    define JB_ESI (1)
#    define JB_EDI (2)
#    define JB_EBP (3)
#    define JB_SP  (4)
#    define JB_PC  (5)

#  elif defined(CONFIG_ARCH_ARM)

/* Storage order v1-v6, sl, fp, sp, lr */

#    define JB_V1  (0)          /* v1 == r4 */
#    define JB_V2  (1)          /* v2 == r5 */
#    define JB_V3  (2)          /* v3 == r6 */
#    define JB_V4  (3)          /* v4 == r7 */
#    define JB_V5  (4)          /* v5 == r8 */
#    define JB_V6  (5)          /* v6 == r9 == sb */
#    define JB_SL  (6)          /* v7 == r10 == sl */
#    define JB_FP  (7)          /* fp == r11 */
#    define JB_SP  (8)          /* sp == r13 */
#    define JB_LR  (9)          /* lr == r14 */

#  endif

/**************************************************************************
 * Public Types
 **************************************************************************/

/* This structure defines the query hea=d for the que at one priority level */

struct U_pthread_queue_s
  {
#  ifdef CONFIG_MEMORY_ALLOCATOR
    void **list;
#  else
    void *list[CONFIG_QUEUE_SIZE];
#  endif
    int length;
  };
typedef struct U_pthread_queue_s U_pthread_queue_t;

/* We keep our own internal version of jmp_buf so that we can be independent
 * of libc.
 */

struct U_pthread_jumpbuf_s
  {
#  if defined(CONFIG_ARCH_X86)
    /* Storage order: %ebx, $esi, %edi, %ebp, sp, and return PC */

    int __jmpbuf[6];
#  elif defined(CONFIG_ARCH_ARM)
    /* Jump buffer contains v1-v6, sl, fp, sp and pc */

#    if defined CONFIG_ARCH_HAS_FPHW
#      warning "Verify your FP hardware requirement"
    int __jmpbuf[18];           /* Plus 4*64-bit registers */
#    else
    int __jmpbuf[10];
#    endif
#  else
#    error "No recognized architecture"
#  endif
  };
typedef struct U_pthread_jumpbuf_s U_pthread_jmpbuf[1];

/* The current state of any thread is given by the following enumeration */

enum U_pthread_state_e
  {
    PTHREAD_STATE_UNINITIALIZED = 0,    /* Thread has never been started */
    PTHREAD_STATE_READYTORUN,   /* Started, initialized and readytorun (or
                                 * running) */
    PTHREAD_STATE_MUTEX_WAITING,        /* Waiting for a mutex */
    PTHREAD_STATE_COND_WAITING, /* Waiting for a condition */
#  ifdef CONFIG_PTHREAD_TIMER
    PTHREAD_STATE_TIMEOUT_WAITING,      /* Waiting for a condition or a timeout 
                                         */
#  endif
    PTHREAD_STATE_JOIN_WAITING, /* Waiting for pthread exit and join */
#  ifdef CONFIG_MQUEUE
    PTHREAD_STATE_MQNFULL_WAITING,      /* Waitign for MQ to be not full */
    PTHREAD_STATE_MQNEMPTY_WAITING,     /* Waiting for MQ to be non empty */
#  endif
#  ifdef CONFIG_SEMAPHORES
    PTHREAD_STATE_SEM_WAITING,  /* Waiting on a semaphore */
#  endif
    PTHREAD_STATE_DEFUNCT,      /* Thread is defunct and waiting pthread_join */
    PTHREAD_STATE_DETACHED      /* Thread is detached */
  };
typedef enum U_pthread_state_e U_pthread_state_t;

/* This is the way we will handle time internally, independent of hosted
 * environments.
 */

#  ifdef CONFIG_PTHREAD_TIMER
struct U_pthread_time_s
  {
    int seconds;                /* Seconds */
    int microseconds;           /* Microseconds */
  };
#  endif

#  ifdef CONFIG_MQUEUE
struct U_mqdes_s;               /* See pthread-types.h */
struct U_mqueue_s;              /* See mqueue-internal.h */
#  endif

#  ifdef CONFIG_SEMAPHORES
struct U_sem_s;                 /* See pthread-types.h */
#  endif

struct U_pthread_internal_s
  {
    U_pthread_state_t state;    /* Current state of the thread */
    int priority;               /* Current thread execution priority */
    unsigned int verification_key;      /* Used to verify if an instance is
                                         * valid */
#  define PTHREAD_VERIFICATION_KEY 0xc0de
    unsigned int flags;         /* See PTHREAD_FLAG_* definitions */

    /* If the thread is blocked on a mutex, condition variable or a join, ... */

    struct U_pthread_mutex_s *mutex;    /* Mutex that we are waiting for OR
                                         * mutex associated with a condition */
    /* The following should be in a union */

    struct U_pthread_cond_s *cond;      /* Condition that we are waiting for */
    struct U_pthread_internal_s *join;  /* Thread that we are waiting to join
                                         * to */
#  ifdef CONFIG_PTHREAD_TIMER
    struct U_pthread_time_s abstime;    /* Wait-for-condition timeout */
    int timedout;               /* 1: timedout */
#  endif
#  ifdef CONFIG_MQUEUE
    struct U_mqueue_s *mqueue;  /* MQ we are waiting (not full or not empty) */
#  endif
#  ifdef CONFIG_SEMAPHORES
    struct U_sem_s *sem;        /* Semaphore that we are waiting for */
#  endif

    /* Thread startup info (not used after thread started) */

    void *arg;                  /* Argument passed when the thread was started */
    void *(*start) (void *arg); /* Entry point to start the threads */

    /* Stack */

#  ifdef CONFIG_MEMORY_ALLOCATOR
    void *stackptr;             /* Pointer to the allocated stack */
    int stacksize;              /* Size of the allocated stack */
#  else
    int stack[CONFIG_DEFAULT_STACKSIZE / sizeof(int)];
#  endif

    /* Thread return value. */

    U_pthread_jmpbuf restart;   /* Used to restart the thread */
    void *retval;               /* Value supplied when the thread exists */

    /* Thread specific data */

#  if CONFIG_NKEYS > 0
    void *data[CONFIG_NKEYS];   /* Array of slots for eacy key value */
#  endif

    /* Message Queue Support */

#  ifdef CONFIG_MQUEUE
    U_pthread_queue_t mqdes_list;       /* List of MQ descriptors */
#  endif
  };
typedef struct U_pthread_internal_s U_pthread_internal_t;

/**************************************************************************
 * Public Variables
 **************************************************************************/

/* Array of ready-to-run thread lists, each representing a different
 * priority level.  Within a priority level, threads are scheduled
 * FIFO.
 */

extern U_pthread_queue_t ready_to_run[PTHREAD_PRIORITY_LEVELS];

/* This is a list of threads that are blocked from execution */

extern U_pthread_queue_t mutex_waiting; /* waiting for a mutex */
extern U_pthread_queue_t cond_waiting;  /* Waiting for a condition */
extern U_pthread_queue_t join_waiting;  /* Waiting for a join */
#  ifdef CONFIG_MQUEUE
extern U_pthread_queue_t mqnfull_waiting;       /* Waiting for MQ to be not
                                                 * full */
extern U_pthread_queue_t mqnempty_waiting;      /* Waiting for MQ to be not
                                                 * empty */
#  endif
#  ifdef CONFIG_SEMAPHORES
extern U_pthread_queue_t sem_waiting;   /* Waiting for a semaphore */
#  endif

/* This is a list of defunct threads.  These are threads that exitted
 * while in the not-detached (attached?) state.  Since they were not
 * detached, we must assume that some other thread will ask for the
 * return value at some point in the future.
 */

extern U_pthread_queue_t defunct;       /* No longer running, but still
                                         * attached */

/* This is the currently active thread */

extern U_pthread_internal_t *current;

#  ifndef CONFIG_MEMORY_ALLOCATOR
#    if !defined CONFIG_MAX_PTHREADS
#      define CONFIG_MAX_PTHREADS 10
#    endif
#  endif

/**************************************************************************
 * Public Function Prototypes
 **************************************************************************/

/* Allocators / deallocators */

#  ifdef CONFIG_MEMORY_ALLOCATOR
void *U_pthread_malloc(unsigned int size);
void *U_pthread_realloc(void *ptr, unsigned int size);
void U_pthread_free(void *ptr);
#  endif

U_pthread_internal_t *U_pthread_allocate(int stacksize);
void U_pthread_deallocate(U_pthread_internal_t * thread);
void U_pthread_deallocate_stack(U_pthread_internal_t * thread);

/* Thread queue management */

void U_pthread_readytorun(U_pthread_internal_t * thread);
void U_pthread_unready(U_pthread_internal_t * thread);
void U_pthread_block(U_pthread_queue_t * queue, U_pthread_internal_t * thread);
void U_pthread_unblock(U_pthread_queue_t * queue,
                       U_pthread_internal_t * thread);
void U_pthread_defunct(U_pthread_internal_t * thread);

void U_pthread_queue_add_tail(U_pthread_queue_t * queue, void *item);
void U_pthread_queue_insert(U_pthread_queue_t * queue, void *item, int index);

void *U_pthread_queue_remove_index(U_pthread_queue_t * queue, int index);
void *U_pthread_queue_remove(U_pthread_queue_t * queue, void *item);
void *U_pthread_queue_remove_head(U_pthread_queue_t * queue);
void *U_pthread_queue_remove_tail(U_pthread_queue_t * queue);

/* Thread state constols */

int U_pthread_setup(U_pthread_internal_t * thread);
int U_pthread_setjmp(U_pthread_jmpbuf env);
void U_pthread_longjmp(U_pthread_jmpbuf env, int val);
void U_pthread_schedule(void);
void U_pthread_checkjoin(U_pthread_internal_t * thread);

/* Mutex / condition variable support */

int U_pthread_mutex_trylock_internal(U_pthread_internal_t * thread,
                                     struct U_pthread_mutex_s *mutex);

#  ifdef CONFIG_PTHREAD_TIMER
int U_pthread_cond_wait_internal(struct U_pthread_cond_s *cond,
                                 struct U_pthread_mutex_s *mutex,
                                 int b_timed, struct U_pthread_time_s *abstime);
int U_pthread_cond_ready(U_pthread_internal_t * thread, int b_timedout);
#  endif

/* Thread utilities */

#  ifdef CONFIG_PTHREAD_TIMER
void U_pthread_time_internal(struct U_pthread_time_s *time);
int U_pthread_compare_time(struct U_pthread_time_s *time1,
                           struct U_pthread_time_s *time2);
#  endif

#  ifdef CONFIG_DEFAULT_LOGGER
int U_pthread_log(const char *format, ...);
#  endif

int U_pthread_valid(const U_pthread_internal_t * internal_thread);
#  if 0
int U_pthread_active(const struct U_pthread_s *thread);
#  endif

#endif                                 /* __PTHREAD_INTERNAL_H */
