
/**************************************************************************
 * pthread-schedule.c
 * Implements U_pthread_schedule.c and supporting functions
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "pthread-internal.h"
#ifdef CONFIG_MQUEUE
#  include "mqueue-internal.h"
#endif

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/* This is the thread structure for the main program thread */

static U_pthread_internal_t main_thread = {
  .state = PTHREAD_STATE_READYTORUN,
  .priority = PTHREAD_PRIORITY_INTERNAL(PTHREAD_PRIORITY_DEFAULT),
  .verification_key = PTHREAD_VERIFICATION_KEY
};

/* This is the currently active thread */

U_pthread_internal_t *current = &main_thread;

/* Array of thread lists, each representing a different priority level: */

U_pthread_queue_t ready_to_run[PTHREAD_PRIORITY_LEVELS];

/* These are lists of threads that are blocked from execution */

U_pthread_queue_t mutex_waiting;        /* waiting for a mutex */
U_pthread_queue_t cond_waiting; /* Waiting for a condition */
U_pthread_queue_t join_waiting; /* Waiting for a join */
#ifdef CONFIG_MQUEUE
U_pthread_queue_t mqnfull_waiting;      /* Waiting for MQ to be not full */
U_pthread_queue_t mqnempty_waiting;     /* Waiting for MQ to be not empty */
#endif
#ifdef CONFIG_SEMAPHORES
U_pthread_queue_t sem_waiting;  /* Waiting for a semaphore */
#endif

/* This is a list of defunct threads.  These are threads that exitted
 * while in the not-detached (attached?) state.  Since they were not
 * detached, we must assume that some other thread will ask for the
 * return value at some point in the future.
 */

U_pthread_queue_t defunct;      /* No longer running, but still attached */
static U_pthread_queue_t detached;

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

void __attribute__ ((constructor)) U_pthread_initialize(void)
{
  U_pthread_readytorun(current);
}

#ifdef CONFIG_PTHREAD_TIMER
static void U_pthread_check_timeouts(void)
{
  struct U_pthread_time_s now;
  int i;

  /* Get the current time */

  U_pthread_time_internal(&now);

  /* Find any threads waiting for a condition timeout and restart them */

  for (i = 0; i < cond_waiting.length; i++)
    {
      U_pthread_internal_t *waiter = cond_waiting.list[i];
      /* Is this thread waiting for for a timeout? If so, is the current time
       * after the restart time (abstime). */

      if (waiter->state == PTHREAD_STATE_TIMEOUT_WAITING &&
          U_pthread_compare_time(&now, &waiter->abstime) > 0)
        {
          /* Yes, wake up the thread (if only just to wait on a mutex) */

          U_pthread_cond_ready(waiter, 1);
        }
    }
}
#else
#  define U_pthread_check_timeouts() do {} while(0)
#endif

/* Find the highest priority thread to execute.  This will be the thread
 * with the entry at the head of the highest priority queue.  Within a
 * queue, threads execute FIFO.
 */

static U_pthread_internal_t *U_pthread_next_to_run(void)
{
  int i;

  /* Search for a ready-to-run thread to execute */

  /* The queues are in priority order.  The next highest priority, ready-to-run
   * thread with be at the head of the first, non-empty queue that we encounter */

  for (i = 0; i <= PTHREAD_PRIORITY_MAX; ++i)
    {
      if (ready_to_run[i].length != 0)
        {
          return ready_to_run[i].list[0];
        }
    }

  return NULL;
}

void
U_pthread_context_switch(U_pthread_internal_t * from, U_pthread_internal_t * to)
{
  /* Save the "from" thread's execution context and context switch to the "to"
   * thread. */

  if (!U_pthread_setjmp(from->restart))
    {
      /* If we get here then we just saved the "from" thread's execution
       * context.  Setjmp will return non-zero in this case. Now, longjmp into 
       * the "to" thread's execution context. */

      current = to;

      U_pthread_longjmp(to->restart, 1);
    }

  /* We get here only when the current thread stops execution (because it
   * blocked, terminated, or yielded). */
}

static void U_pthread_dispose_detached(void)
{
  U_pthread_internal_t *thread = U_pthread_queue_remove_tail(&detached);

  if (thread != NULL)
    {
      U_pthread_deallocate(thread);
    }
}

/**************************************************************************
 * Public Functions
 **************************************************************************/

void U_pthread_schedule(void)
{
  U_pthread_internal_t *next;

  switch (current->state)
    {
    case PTHREAD_STATE_READYTORUN:
      /* The thread is ready to run, remove the thread from the ready to run
       * list. */

      U_pthread_unready(current);

      /* Then reinsert the thread into the priority queue but at the end.
       * Since threads execute FIFO, this effectively implements yield to
       * higher priority threads and to other threads of the same priority. */

      U_pthread_readytorun(current);
      break;

    case PTHREAD_STATE_DEFUNCT:
      /* The defunct thread is still in the ready to run list. Remove the
       * defunct thread from the ready to run list and dispose of it. */

      U_pthread_unready(current);
      U_pthread_defunct(current);
      break;

    default:
      break;
    }

  do
    {
      /* Check for timed condition variable wait timeouts */

      U_pthread_check_timeouts();

      /* Get the highest priority thread to execute */
    }
  while ((next = U_pthread_next_to_run()) == NULL);

  /* Dispatch the current thread and handle its return condition */

  U_pthread_context_switch(current, next);

  /* Dispose a detached thread if any */

  U_pthread_dispose_detached();
}

void U_pthread_defunct(U_pthread_internal_t * thread)
{
  /* Close all opened message queue descriptors and, potentially, free all
   * stranded messages. NOTE: Unfortunately this will drag in a lot of mqueue
   * logic unconditionally.  That is an unfortunate but necessary coupling that 
   * can be avoided only be */

#ifdef CONFIG_MQUEUE
  /* Close the MQ descriptor, dispose of any stranded messages, and remove the
   * descriptor from the list of descriptors. */

  U_mqueue_close_all(thread);
#endif

  /* If the thread has been detached we can deallocate it now. */

  if (thread->flags & PTHREAD_FLAG_DETACHED)
    {
      /* Check if this completes a join.  This shouldn't happen but do this to
       * make sure.  The join logic should see the detached indication and do
       * no more than terminate with an error. */

      U_pthread_checkjoin(thread);

      U_pthread_deallocate_stack(thread);
      thread->state = PTHREAD_STATE_DETACHED;
      thread->verification_key = 0;

      U_pthread_queue_add_tail(&detached, thread);
    }
  else
    {
      /* Otherwise, we have to hold it for a bit in the defunct list.  Since
       * the thread is not detached then either someone is being sloppy or else 
       * the there is someone waiting to get the thread's return value (via
       * pthread_join()). */

      U_pthread_queue_add_tail(&defunct, thread);

      /* Check if this completes a join.  This is the normal case. In this
       * case, the join logic will remove the thread from the defunct list and
       * discard it. */

      U_pthread_checkjoin(thread);
    }
}
