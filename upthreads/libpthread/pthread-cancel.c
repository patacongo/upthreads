
/**************************************************************************
 * pthread-cancel.c
 * Implements pthread_cancel();
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "pthread-internal.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

/**************************************************************************
 * Public Functions
 **************************************************************************/

int U_pthread_cancel(U_pthread_t thread)
{
  U_pthread_internal_t *internal_thread = (U_pthread_internal_t *) thread;

  dbg("pthread_cancel(%p)\n", thread);

  /* First, make sure that the handle references a valid thread */

  if (!U_pthread_valid(internal_thread))
    {
      /* No thread could be found corresponding to that specified by the thread 
       * ID. */

      return ESRCH;
    }

  /* Check to see if this thread has the non-cancelable bit set in its flags. */

  if ((internal_thread->flags & PTHREAD_FLAG_NONCANCELABLE) != 0)
    {
      /* Then we cannot cancel the thread */

      return ESRCH;
    }

  /* Check to see if the ID refers to ourselves.. this would be the same as
   * pthread_exit(PTHREAD_CANCELED). */

  if (internal_thread == current)
    {
      pthread_exit(PTHREAD_CANCELED);
      return 0;
    }

  /* Nope... we'll have to search for the thread.  We'll assume that everything 
   * is healthy and use the thread state to determine where to find the thread. 
   * This could only fail if there is some major queue screw-up or, more
   * likely, someone changes something in the future and forgets to fix this
   * logic. */

  switch (internal_thread->state)
    {
    case PTHREAD_STATE_READYTORUN:     /* Started, initialized and readytorun
                                         * (or running) */
      U_pthread_unready(internal_thread);
      break;

    case PTHREAD_STATE_MUTEX_WAITING:  /* Thread is waiting for a mutex */
      U_pthread_unblock(&mutex_waiting, internal_thread);
      break;

    case PTHREAD_STATE_COND_WAITING:   /* Thread is waiting for a condition */
#ifdef CONFIG_PTHREAD_TIMER
    case PTHREAD_STATE_TIMEOUT_WAITING:        /* Thread is waiting for a
                                                 * condition or a timeout */
      U_pthread_unblock(&cond_waiting, internal_thread);
      break;
#endif
    case PTHREAD_STATE_JOIN_WAITING:   /* Thread is waiting for pthread exit
                                         * and join */
      U_pthread_unblock(&join_waiting, internal_thread);
      break;

#ifdef CONFIG_MQUEUE
    case PTHREAD_STATE_MQNFULL_WAITING:        /* Waitign for MQ to be not
                                                 * full */
      U_pthread_unblock(&mqnfull_waiting, internal_thread);
      break;

    case PTHREAD_STATE_MQNEMPTY_WAITING:       /* Waiting for MQ to be non
                                                 * empty */
      U_pthread_unblock(&mqnempty_waiting, internal_thread);
      break;
#endif

#ifdef CONFIG_SEMAPHORES
    case PTHREAD_STATE_SEM_WAITING:    /* Waitign for a semaphore */
      U_pthread_unblock(&sem_waiting, internal_thread);
      break;
#endif

    case PTHREAD_STATE_UNINITIALIZED:  /* Thread has never been started */
    case PTHREAD_STATE_DEFUNCT:        /* Thread is defunct and waiting to be
                                         * disposed of */
    case PTHREAD_STATE_DETACHED:       /* Thread is defunct but waiting for
                                         * pthread_join */
    default:
      /* No thread could be found corresponding to that specified by the thread 
       * ID. */

      return ESRCH;
    }

  /* Okay, the thread has been removed from its queue, now dispose of it. */

  internal_thread->state = PTHREAD_STATE_DEFUNCT;
  internal_thread->retval = PTHREAD_CANCELED;
  U_pthread_defunct(internal_thread);

  return 0;
}
