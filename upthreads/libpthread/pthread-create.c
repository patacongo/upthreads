
/**************************************************************************
 * pthread-create.c
 * Implements the public API pthread_create()
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "pthread-internal.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

/**************************************************************************
 * Public Functions
 **************************************************************************/

/* Create a new, ready-to-run thread */

int
U_pthread_create(U_pthread_t * thread, U_pthread_attr_t * attr,
                 void *(*start_routine) (void *), void *arg)
{
  U_pthread_internal_t *internal_thread;
  int priority;
  int stacksize;

  /* If no attributes were provided, we will use a default halfway between the 
   * min and max. */

  if (!attr)
    {
      priority = PTHREAD_PRIORITY_INTERNAL(PTHREAD_PRIORITY_DEFAULT);
      stacksize = CONFIG_DEFAULT_STACKSIZE;
    }
  else
    {
      /* Otherwise, we will convertion the sched_priority into our internal
       * priority (i.e., execution priority decreasing with increasing priority
       * numbers. */

      /* Get and verify the priority in the attributes */

      priority = attr->sched_priority;
      if (priority > PTHREAD_PRIORITY_MAX)
        {
          /* Too big? Clip to the maximum */

          priority = PTHREAD_PRIORITY_MAX;
        }
      else if (priority < PTHREAD_PRIORITY_MIN)
        {
          /* Too small? Clip it to the maximum */

          priority = PTHREAD_PRIORITY_MIN;
        }
      else
        {
          /* Otherwise, perform the conversion */

          priority = PTHREAD_PRIORITY_INTERNAL(priority);
        }

      /* Get and verify the stacksize in the attributes */

      stacksize = attr->stacksize;
      if (stacksize <= 0)
        {
          stacksize = CONFIG_DEFAULT_STACKSIZE;
        }
    }

  dbg("pthread_create(): priority=%d stacksize=%d\n", priority, stacksize);

  /* Allocate an internal pthread structure.  The allocator will clear the new
   * structure, allocate a stack and set the verification_key. */

  internal_thread = U_pthread_allocate(stacksize);

  if (!internal_thread)
    {
      return -1;
    }

  /* Initialize the non-zero elements of alloated thread structure */

  internal_thread->state = PTHREAD_STATE_READYTORUN;
  internal_thread->priority = priority;
  internal_thread->arg = arg;
  internal_thread->start = start_routine;
  internal_thread->verification_key = PTHREAD_VERIFICATION_KEY;

  /* Initialize the structure so that it is ready to run */

  if (U_pthread_setup(internal_thread) != 0)
    {
      U_pthread_deallocate(internal_thread);
      return -1;
    }

  /* Insert the iniatlized thread structure into the priority queue. */

  U_pthread_readytorun(internal_thread);
  dbg2("pthread_create: Started thread %p\n", internal_thread);

  /* If we just create a thread that is higher priority than ourself, then we
   * need to run that thread now (unless the scheduler is locked) */

  if (internal_thread->priority < current->priority &&
      (current->flags & PTHREAD_FLAG_SCHEDLOCK) == 0)
    {
      U_pthread_schedule();
    }

  /* Did the caller provide a container to receive the thread information? */

  if (thread)
    {
      /* Yes, Provide a handle to the new thread */

      *thread = internal_thread;
    }

  return 0;
}
