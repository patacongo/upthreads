
/**************************************************************************
 * pthread-cond-signal.c
 * Implements pthread_cond_signal() and pthread_cond_broadcast()
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "pthread-internal.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

#ifndef CONFIG_PTHREAD_TIMER
static int U_pthread_cond_ready(U_pthread_internal_t * thread);
#endif

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

static int U_pthread_cond_signal_internal(U_pthread_cond_t * cond, int b_all)
{
  int reschedule = 0;
  int i;

  /* Note that since we are, in reality, single threaded this really pretty
   * simple stuff. */

  if (!cond)
    {
      return EINVAL;
    }

  dbg3("pthread_cond_signal_internal: current=%p signals condition\n", current);

  /* Find the highest priority thread waiting for this cond (if any) */

  for (i = 0; i < cond_waiting.length; i++)
    {
      U_pthread_internal_t *waiter = cond_waiting.list[i];

      /* Is this thread waiting for this cond? */

      if ((waiter->state == PTHREAD_STATE_COND_WAITING ||
           waiter->state == PTHREAD_STATE_TIMEOUT_WAITING) &&
          waiter->cond == cond)
        {
          /* Yes, wake up the thread (if only just to wait on a mutex) */

          int result = U_pthread_cond_ready(waiter, 0);

          if (result)
            {
              reschedule = 1;
            }

          /* Are we broadcasting to all threads? Or signalling on one receiver? 
           */

          if (!b_all)
            {
              /* Just one -- we are done */

              if (reschedule && (current->flags & PTHREAD_FLAG_SCHEDLOCK) == 0)
                {
                  U_pthread_schedule();
                }
              return 0;
            }
        }
    }

  /* Return success -- we get here if no waiter was signaled or if all waiters
   * were signalled. */

  if (reschedule && (current->flags & PTHREAD_FLAG_SCHEDLOCK) == 0)
    {
      U_pthread_schedule();
    }

  return 0;
}

/**************************************************************************
 * Public Functions
 **************************************************************************/

/* This function is called in two contexts:  (1) when the condition is
 * signal via pthread_cond_signal() pthread_cond_broadcast(), or (2) after
 * a timeout occurs.  The difference is that current will be NULL in the
 * latter case.
 */

int U_pthread_cond_ready(U_pthread_internal_t * thread, int b_timedout)
{
  int reschedule = 0;

  /* return the result -- either the condition is signal or we timedout */

  if (b_timedout)
    {
      thread->cond->signaled = 0;
      thread->timedout = 1;
    }
  else
    {
      thread->cond->signaled = 1;
      thread->timedout = 0;
    }

  /* Remove the waiting thread from the blocked list */

  U_pthread_unblock(&cond_waiting, thread);

  /* Does the thread need to have the mutex associated with the condition code? 
   * If so we'll need to do that here so that the transition from waiting for
   * condition to the mutex operation is atomic. */

  if (thread->mutex)
    {
      /* We need the mutex.  Try to get it now.
       * U_pthread_mutex_trylock_internal may return success, or an error.  The 
       * special error of EBUSY means that everything is okay BUT we cannot
       * have the mutex now because another thread holds it.  On this special
       * error, we will put the thread thread into the waiting-for-mutex state. 
       * Otherwise, it is ready to run. */

      int status = U_pthread_mutex_trylock_internal(thread, thread->mutex);
      if (status == EBUSY && thread->mutex->holder != thread)
        {
          dbg2("pthread_cond_ready: Waiting for mutex, thread=%p\n", thread);

          /* Mark the thread waiting for a mutex */

          thread->state = PTHREAD_STATE_MUTEX_WAITING;

          /* Add the thread to a blocked list */

          U_pthread_block(&mutex_waiting, thread);
        }
      else
        {
          /* We have the mutex (or we are never going to get it). Put the
           * thread directly into the ready-to-run state. Note that we do not
           * give control to the unblocked thread until the current thread
           * returns. */

          dbg2("pthread_cond_signal: Waking up %p with mutex (or error)\n",
               thread);
          thread->state = PTHREAD_STATE_READYTORUN;
          U_pthread_readytorun(thread);

          /* Check if we should reschedule */

          if (current && thread->priority > current->priority)
            {
              reschedule = 1;
            }
        }
    }
  else
    {
      /* We are not waiting for a mutex, put the thread directly into the
       * ready-to-run state. Note that we do not give control to the unblocked
       * thread until the current thread returns. */

      dbg2("pthread_cond_signal: Waking up %p without mutex\n", thread);
      thread->state = PTHREAD_STATE_READYTORUN;
      U_pthread_readytorun(thread);

      /* Check if we should reschedule */

      if (current && thread->priority > current->priority)
        {
          reschedule = 1;
        }
    }

  return reschedule;
}

int U_pthread_cond_signal(U_pthread_cond_t * cond)
{
  return U_pthread_cond_signal_internal(cond, 0);
}

int U_pthread_cond_broadcast(U_pthread_cond_t * cond)
{
  return U_pthread_cond_signal_internal(cond, 1);
}
