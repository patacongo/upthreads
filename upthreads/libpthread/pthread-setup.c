
/**************************************************************************
 * pthread_setup.c
 * Implements U_pthread_setup and U_pthread_setup
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include <string.h>             /* memset */

#include "pthread-internal.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

static void U_pthread_trampoline(void)
{
  void *retval;

  dbg("pthread_trampoline()\n", current);

  /* Call into the pthread at its entry point */

  retval = current->start(current->arg);

  /* if it returns, then call pthread_exit on behalf of the thread */

  pthread_exit(retval);
}

/* On the x86, the stack grows toward lower address (push down) and
 * each stack element is 4-bytes wide.  The stack is pre-decremented
 * before each push and post-increment after each pop.
 */

#if defined(CONFIG_ARCH_X86) || defined(CONFIG_ARCH_ARM)
static unsigned int U_pthread_topofstack(U_pthread_internal_t * thread)
{
#  ifdef CONFIG_MEMORY_ALLOCATOR
  /* This is the address of the last byte in the allocation */

  unsigned int stackaddress =
    (unsigned int)thread->stackptr + thread->stacksize - 1;

  /* return the last aligned word in the allocation.  This will be the top of
   * the push-down stack */

  return stackaddress & ~3;
#  else
  return (unsigned int)&thread->stack[CONFIG_DEFAULT_STACKSIZE /
                                      sizeof(int) - 1];
#  endif
}
#endif

/**************************************************************************
 * Public Functions
 **************************************************************************/

int U_pthread_setup(U_pthread_internal_t * thread)
{
  /* Some sanity checking */

#ifdef CONFIG_MEMORY_ALLOCATOR
  if (!thread || !thread->restart || !thread->start || !thread->stackptr)
#else
  if (!thread || !thread->restart || !thread->start)
#endif
    {
      err("U_pthread_setup: bad parameters\n");
      return EINVAL;
    }

  memset(thread->restart, 0, sizeof(U_pthread_jmpbuf));
#ifdef CONFIG_ARCH_X86
  thread->restart->__jmpbuf[JB_SP] = U_pthread_topofstack(thread);
  thread->restart->__jmpbuf[JB_PC] = (int)U_pthread_trampoline;
#elif defined(CONFIG_ARCH_ARM)
  thread->restart->__jmpbuf[JB_SP] = U_pthread_topofstack(thread);
  thread->restart->__jmpbuf[JB_LR] = (int)U_pthread_trampoline;
#else
#  error "No recognized architecture"
#endif

  return 0;
}
