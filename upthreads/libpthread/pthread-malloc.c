
/**************************************************************************
 * pthread-malloc.c
 * Implements pthread-malloc() and pthread_free(), wrappers for malloc and
 * free
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "pthread-config.h"

#ifdef CONFIG_MEMORY_ALLOCATOR

/**************************************************************************
 * Private Definitions
 **************************************************************************/

#  ifndef CONFIG_MALLOC
#    include <stdlib.h>
#    define CONFIG_MALLOC malloc
#  else
extern void *CONFIG_MALLOC(unsigned int size);
#  endif

#  ifndef CONFIG_REALLOC
#    include <stdlib.h>
#    define CONFIG_REALLOC realloc
#  else
extern void *CONFIG_REALLOC(void *ptr, unsigned int size);
#  endif

#  ifndef CONFIG_FREE
#    include <stdlib.h>
#    define CONFIG_FREE free
#  else
extern void CONFIG_FREE(void *ptr);
#  endif

#  include "pthread-internal.h"

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

/**************************************************************************
 * Public Functions
 **************************************************************************/

void *U_pthread_malloc(unsigned int size)
{
  return CONFIG_MALLOC(size);
}

void *U_pthread_realloc(void *ptr, unsigned int size)
{
  return CONFIG_REALLOC(ptr, size);
}

void U_pthread_free(void *ptr)
{
  CONFIG_FREE(ptr);
}
#endif
