
/**************************************************************************
 * pthread-specific
 * Support for thread specific data
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "pthread-internal.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/* This is the number of keys available to be allocated */

static int pthread_num_keys = 0;

/**************************************************************************
 * Private Functions
 **************************************************************************/

/**************************************************************************
 * Public Functions
 **************************************************************************/

/**************************************************************************
 * Function:  U_pthread_key_create
 *
 * Description:
 *   This function creates a thread-specific data key visible to all
 *   threads in the system.  Although the same key value may be used by
 *   different threads, the values bound to the key by pthread_setspecific()
 *   are maintained on a per-thread basis and persist for the life of the
 *   calling thread.
 *
 *   Upon key creation, the value NULL will be associated with the the new
 *   key in all active threads.  Upon thread creation, the value NULL will
 *   be associated with all defined keys in the new thread.
 *
 * Parameters:
 *   key = A pointer to the key to create.
 *   destructor = An optional destructor() function that may be associated
 *      with each key that is invoked when a thread exits.  However, this
 *      argument is ignored in the current implementation.
 *
 * Return Value:
 *   If successful, the pthread_create() function will store the newly
 *   created key value at *key and return zero.  Otherwise, an error number
 *   will be returned to indicate the error:
 *
 *      EAGAIN - The system lacked sufficient resources to create another
 *         thread-specific data key, or the system-imposed limit on the
 *         total number of keys per process {PRTHREAD_KEYS_MAX} has been
 *         exceeded
 *      ENOMEM - Insufficient memory exists to create the key.
 *
 **************************************************************************/

int U_pthread_key_create(U_pthread_key_t * key, void (*destructor) (void *))
{
#if PTHREAD_KEYS_MAX > 0
  int retval = EAGAIN;

  /* Check if we have exceeded the system-defined number of keys. */

  if (pthread_num_keys < PTHREAD_KEYS_MAX)
    {
      /* Return the key value and increment the count of global keys */

      *key = pthread_num_keys++;
      retval = 0;
    }

  return retval;
#else
  return EAGAIN;
#endif
}

/**************************************************************************
 * Function:  U_pthread_setspecific & U_pthread_getspecific
 *
 * Description:
 *   The pthread_setspecific() function associates a thread-specific value
 *   with a key obtained via a previous call to pthread_key_create().
 *   Different threads may bind different values to the same key.  These
 *   values are typically pointers to blocks of dynamically allocated
 *   memory that have been reserved for use by the calling thread.
 *
 *   The pthread_getspecific() function returns the value currently bound
 *   to the specified key on behalf of the calling thread.
 *
 *   The effect of calling pthread_setspecific() or pthread_getspecific()
 *   with a key value not obtained from pthread_key_create() or after a key
 *   has been deleted with pthread_key_delete() is undefined.
 *
 * Parameters:
 *   key = The data key to get or set
 *   value = On set, the value to bind to the key.
 *
 * Return Value:
 *   The function pthread_getspecific() returns the thread-specific data
 *   associated with the given key.  If no thread specific data is associated
 *   with the key, then the value NULL is returned.
 *
 *   If successful, pthread_setspecific() will return zero. Otherwise, an
 *   error number will be returned:
 *
 *      ENOMEM - Insufficient memory exists to associate the value with the key.
 *      EINVAL - The key value is invalid.
 *
 **************************************************************************/

int U_pthread_setspecific(U_pthread_key_t key, void *value)
{
#if PTHREAD_KEYS_MAX > 0
  int retval = EINVAL;

  dbg("pthread_setspecific(%d, %p)\n", key, value);

  /* Check if the key is valid. */

  if (key < pthread_num_keys)
    {
      /* Store the data in the thread structure. */

      current->data[key] = value;
      retval = 0;
    }
  return retval;
#else
  return EINVAL;
#endif
}

void *U_pthread_getspecific(U_pthread_key_t key)
{
#if PTHREAD_KEYS_MAX > 0
  void *retval = NULL;

  /* Check if the key is valid. */

  if (key < pthread_num_keys)
    {
      /* Return the stored value. */

      retval = current->data[key];
    }

  dbg("pthread_getspecific(%d) retval=%p\n", key, retval);

  return retval;
#else
  return NULL;
#endif
}

/**************************************************************************
 * Function:  U_pthread_key_delete
 *
 * Description:
 *   This POSIX function should delete a thread-specific data key
 *   previously returned by pthread_key_create().  However, this function
 *   does nothing in the present implementation.
 *
 * Parameters:
 *   key = the key to delete
 *
 * Return Value:
 *   Always returns ENOSYS.
 *
 **************************************************************************/

int U_pthread_key_delete(U_pthread_key_t key)
{
  return ENOSYS;
}
