
/**************************************************************************
 * pthread_clock.c
 * Implements U_pthread_clock
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "pthread-config.h"

/* In order to use pthread_cond_timedwait, the CONFIG_PTHREAD_TIMER
 * must be enabled.  If enabled, the configuration also has the option to
 * provide:
 *
 * CONFIG_PTHREAD_USCLOCK -- prototype type unsigned int clock(void).
 *   returns the current time in ticks. If CONFIG_PTHREAD_TIMER is
 *   defined but CONFIG_PTHREAD_USCLOCK is not, the Linux clock() function
 *   will be used.  Both clocks are assumed to return the running
 *   time in microseconds
 */

#ifdef CONFIG_PTHREAD_TIMER
#  include "pthread-internal.h"
#  ifndef CONFIG_PTHREAD_USCLOCK
#    include <time.h>           /* clock() prototype and CLOCKS_PER_SEC */
#    define CONFIG_PTHREAD_USCLOCK clock
#    if !defined(CLOCKS_PER_SEC) || CLOCKS_PER_SEC != 1000000
#      warning "CLOCKS_PER_SEC assumed to be 1000000"
#    endif
#  else
extern unsigned int CONFIG_PTHREAD_USCLOCK(void);
#  endif

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

static unsigned int last_time;
static struct U_pthread_time_s overflow;

/**************************************************************************
 * Private Functions
 **************************************************************************/

static void
U_pthread_add_time(struct U_pthread_time_s *term1,
                   struct U_pthread_time_s *term2, struct U_pthread_time_s *sum)
{
  /* Add the two times */

  sum->seconds = term1->seconds + term2->seconds;
  sum->microseconds = term1->microseconds + term2->microseconds;

  /* Handle overflow from microseconds to seconds.  This really should only
   * execute on time through the loop. */

  while (sum->microseconds > 1000000)
    {
      sum->seconds++;
      sum->microseconds -= 1000000;
    }
}

static void
U_pthread_convert_time(unsigned int itime, struct U_pthread_time_s *stime)
{
  struct U_pthread_time_s now;
  now.seconds = itime / 1000000;
  now.microseconds = itime - now.seconds * 1000000;
  U_pthread_add_time(&now, &overflow, stime);
}

/**************************************************************************
 * Public Functions
 **************************************************************************/

void U_pthread_time_internal(struct U_pthread_time_s *time)
{
  /* The range of a 32-bit time running at 1MHz is about 2**32/1000000 = 4295
   * seconds or about 72 hours. */

  unsigned int now = CONFIG_PTHREAD_USCLOCK();
  if (now < last_time)
    {
      /* Time can only go backward on an overflow. One bit of overflow is equal 
       * to 2**32 ticks or 4294 seconds + 967296 microseconds. */
      struct U_pthread_time_s incr = { 4294, 967296 };
      U_pthread_add_time(&overflow, &incr, &overflow);
    }

  last_time = now;

  /* Now return the converted time (including any overflow) */

  U_pthread_convert_time(now, time);
}

void U_pthread_time(struct timespec *time)
{
  if (time)
    {
      struct U_pthread_time_s internal_time;
      U_pthread_time_internal(&internal_time);
      time->tv_sec = internal_time.seconds;
      time->tv_nsec = 1000 * internal_time.microseconds;
    }
}

int
U_pthread_compare_time(struct U_pthread_time_s *time1,
                       struct U_pthread_time_s *time2)
{
  if (time1->seconds > time2->seconds)
    return 1;
  else if (time1->seconds < time2->seconds)
    return -1;
  else if (time1->microseconds > time2->microseconds)
    return 1;
  else if (time1->microseconds < time2->microseconds)
    return -1;
  else
    return 0;
}

#endif                                 /* CONFIG_PTHREAD_TIMER */
