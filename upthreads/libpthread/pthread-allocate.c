
/**************************************************************************
 * pthread-allocate.c
 * Implements U_pthread_allocate() and U_pthread__deallocate()
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include <string.h>             /* memset */

#include "pthread-internal.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

#ifndef CONFIG_MEMORY_ALLOCATOR

static U_pthread_queue_t pthread_free_list;

/* Fixed thread pool */

static U_pthread_internal_t preallocated_threads[CONFIG_MAX_PTHREADS];

/* Number of threads not yet allocated from thread pool: */

static unsigned int threads_remaining = CONFIG_MAX_PTHREADS;
#endif

/**************************************************************************
 * Private Functions
 **************************************************************************/

#ifdef CONFIG_MEMORY_ALLOCATOR
static U_pthread_internal_t *U_pthread_allocate_control_block(void)
{
  return U_pthread_malloc(sizeof(U_pthread_internal_t));
}

static void U_pthread_deallocate_control_block(U_pthread_internal_t * thread)
{
  U_pthread_free(thread);
}

static int
U_pthread_allocate_stack(U_pthread_internal_t * thread, int stacksize)
{
  /* allocate a stack */

  thread->stackptr = U_pthread_malloc(stacksize);

  if (!thread->stackptr)
    {
      return -1;
    }

  thread->stacksize = stacksize;
  return 0;
}

void U_pthread_deallocate_stack(U_pthread_internal_t * thread)
{
  if (thread->stackptr)
    {
      U_pthread_free(thread->stackptr);
      thread->stackptr = NULL;
    }
}
#else
static U_pthread_internal_t *U_pthread_allocate_control_block(void)
{
  U_pthread_internal_t *thread = NULL;

  /* Are there any threads in the free list */

  if ((thread = U_pthread_queue_remove_tail(&pthread_free_list)) != NULL)
    {
      /* Yes.. take the thread from the free list */

      return thread;
    }

  if (threads_remaining > 0)
    {
      --threads_remaining;
      return preallocated_threads + threads_remaining;
    }

  return NULL;
}

static void U_pthread_deallocate_control_block(U_pthread_internal_t * thread)
{
  U_pthread_queue_add_tail(&pthread_free_list, thread);
}

static int
U_pthread_allocate_stack(U_pthread_internal_t * thread, int stacksize)
{
  /* do nothing because the stack is in thread control block */
  return 0;
}

void U_pthread_deallocate_stack(U_pthread_internal_t * thread)
{
  /* do nothing because the stack is in thread control block */
}
#endif

/**************************************************************************
 * Public Functions
 **************************************************************************/

/* Allocate a U_pthread_internal_t */

U_pthread_internal_t *U_pthread_allocate(int stacksize)
{
  U_pthread_internal_t *thread = NULL;

  thread = U_pthread_allocate_control_block();

  if (!thread)
    {
      err("pthread_allocate: Thread allocation failed\n");
      return NULL;
    }

  /* Zero the new thread structure */

  memset(thread, 0, sizeof(U_pthread_internal_t));

  /* Allocate the stack for the thread (if any stack was requested */

  if (stacksize)
    {
      if (U_pthread_allocate_stack(thread, stacksize))
        {
          err("pthread_allocate: Stack allocation failed\n");
          U_pthread_deallocate(thread);
          return NULL;
        }
    }

  return thread;
}

void U_pthread_deallocate(U_pthread_internal_t * thread)
{
  dbg("pthread_deallocate(%p)\n", thread);

  if (thread)
    {
      U_pthread_deallocate_stack(thread);
      U_pthread_deallocate_control_block(thread);
    }
}
