
/**************************************************************************
 * pthread-mutex-lock.c
 * Implements pthread_mutex_lock() and pthread_mutex_trylock()
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "pthread-internal.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

/**************************************************************************
 * Public Functions
 **************************************************************************/

int U_pthread_mutex_lock(U_pthread_mutex_t * mutex)
{
  int retval;

  dbg("pthread_mutex_lock(%p)\n", mutex);

  /* Loop until we get the mutex or an error occurs */

  do
    {
      /* Try to get the mutex */

      retval = U_pthread_mutex_trylock_internal(current, mutex);

      /* EBUSY is returned if we could not get the mutex because some other has 
       * it.  In this case, we may have to wait. */

      if (retval == EBUSY)
        {
          /* Make sure that the thread is not ourselves -- waiting in that
           * condition would be a deadlock */

          if (mutex->holder == current)
            {
              dbg("pthread_mutex_lock: Avoiding deadlock, current=%p\n",
                  current);
              retval = EDEADLK;

            }
          else
            {
              /* Otherwise, wait for the mutex to be released */

              dbg2("pthread_mutex_lock: Waiting for mutex, current=%p\n",
                   current);

              /* Mark the thread waiting for a mutex */

              current->state = PTHREAD_STATE_MUTEX_WAITING;
              current->mutex = mutex;

              /* Remove the current thread from the ready-to-run lists */

              U_pthread_unready(current);

              /* Add it to a blocked list and return to the scheduler */

              U_pthread_block(&mutex_waiting, current);
              U_pthread_schedule();

              /* We are back.. we should have the mutex.  If not, we'll try
               * again -- we should always have the mutex when we are awakened. */

              if (mutex->locked && mutex->holder == current)
                {
                  retval = 0;   /* Got it */
                }

              /* Better try again (this should not happen!) */
            }
        }
    }
  while (retval == EBUSY);

  return retval;
}

int
U_pthread_mutex_trylock_internal(U_pthread_internal_t * thread,
                                 struct U_pthread_mutex_s *mutex)
{
  U_pthread_internal_t *holder;

  /* Note that since we are, in reality, single threaded this really pretty
   * simple stuff. */

  if (!mutex)
    {
      return EINVAL;
    }

  holder = mutex->holder;

  /* The common case occurs when the mutex is unlocked (mutex->locked < 1). We
   * also try to recover from some potential error conditions here -- no holder 
   * of the lock (!mutex->holder.internal_thread), or theholder is defunct
   * (verification_keys don't match).  These are all different ways that the
   * lock can be available. */

  if (mutex->locked <= 0 || !holder)
    {
      dbg2("pthread_mutex_trylock_internal: Giving mutex to thread=%p\n",
           thread);
      mutex->locked = 1;
      mutex->holder = thread;
      return 0;
    }

  /* The mutex is recursive and is held by ourselves */

  if (mutex->type == PTHREAD_MUTEX_RECURSIVE && holder == thread)
    {
      mutex->locked++;
      return 0;
    }

  /* Otherwise, the mutex is held by a real thread -- return EBUSY now */

  return EBUSY;
}

int U_pthread_mutex_trylock(U_pthread_mutex_t * mutex)
{
  return U_pthread_mutex_trylock_internal(current, mutex);
}
