
/**************************************************************************
 * pthread-cond-wait.c
 * Implements pthread_cond_wait()
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "pthread-internal.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

/**************************************************************************
 * Public Functions
 **************************************************************************/

int U_pthread_cond_wait(U_pthread_cond_t * cond, U_pthread_mutex_t * mutex)
{
  return U_pthread_cond_wait_internal(cond, mutex, 0, NULL);
}

int
U_pthread_cond_wait_internal(struct U_pthread_cond_s *cond,
                             U_pthread_mutex_t * mutex,
                             int b_timed, struct U_pthread_time_s *abstime)
{
  int mutex_retval;

  if (!cond)
    {
      return EINVAL;
    }

  /* Check if the condition has been signaled.  At this point, we hold the
   * mutex so this should be okay. */

  if (cond->signaled)
    {
      /* Yep, return success */

      cond->signaled = 0;
      return 0;
    }

  /* Otherwise, wait for the condition */

  dbg2("pthread_cond_wait: Waiting for cond, current=%p\n", current);

  /* Remove the current thread from the ready-to-run lists */

  U_pthread_unready(current);

  /* Mark the thread waiting for a cond */

  if (b_timed && abstime)
    {
      current->state = PTHREAD_STATE_TIMEOUT_WAITING;
      current->abstime = *abstime;
    }
  else
    {
      current->state = PTHREAD_STATE_COND_WAITING;
    }

  current->timedout = 0;
  current->cond = cond;
  current->mutex = mutex;

  /* Add it to a wait list and return to the scheduler */

  U_pthread_block(&cond_waiting, current);

  /* Release the mutex and return to the scheduler.  Note that we catch errors
   * from pthread_mutex_lock but don't do anything. pthread_mutex_lock could
   * fail for a number of reasons -- the most likely being that the caller does 
   * not hold the mutex.  We just need to remember this fact so that later we
   * will not whether or not we need to reacquire the mutex. */

  mutex_retval = pthread_mutex_unlock(mutex);
  if (mutex_retval != 0)
    {
      /* We failed to unlock the mutex.  This means that there is a the signal
       * unlock logic will be unable to reacquire it for us. In a real
       * mutli-threaded environment, this could be a race because the condition 
       * might have already been signaled and we might be back in the ready-run 
       * state. */

      current->mutex = NULL;
    }

  /* Return the scheduler.  We will not be restarted until the condition has
   * been signaled. */

  U_pthread_schedule();

  /* We are back.. the condition must have been signaled */

  cond->signaled = 0;           /* Don't check -- may be zero on broadcast */
  return 0;
}
