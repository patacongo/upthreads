
/**************************************************************************
 * Filename
 * Brief Description
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "pthread-internal.h"

/* In order to use pthread_cond_timedwait, the CONFIG_PTHREAD_TIMER
 * must be enabled.  If enabled, the configuration also has the option to
 * provide:
 *
 * CONFIG_PTHREAD_USCLOCK -- prototype type unsigned int clock(void).
 *   returns the current time in ticks. If CONFIG_PTHREAD_TIMER is
 *   defined but CONFIG_PTHREAD_USCLOCK is not, the Linux clock() function
 *   will be used.  Both clocks are assumed to return the running
 *   time in microseconds
 */

#ifdef CONFIG_PTHREAD_TIMER
#  ifdef CONFIG_HAVE_TIME_H
#    include <time.h>
#  endif

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

/**************************************************************************
 * Public Functions
 **************************************************************************/

int
pthread_cond_timedwait(pthread_cond_t * cond, pthread_mutex_t * mutex,
                       const struct timespec *abstime)
{
  struct U_pthread_time_s timeout;

  /* Sanity checking */

  if (!cond || !abstime)
    {
      return EINVAL;
    }

  /* Convert to a 1uS timer */

  timeout.seconds = abstime->tv_sec;
  timeout.microseconds = abstime->tv_nsec / 1000;
  return U_pthread_cond_wait_internal(cond, mutex, 1, &timeout);
}

#endif                                 /* CONFIG_PTHREAD_TIMER */
