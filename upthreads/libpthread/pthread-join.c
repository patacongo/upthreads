
/**************************************************************************
 * pthread-join.c
 * Implements pthread_join()
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "pthread-internal.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

static int U_pthread_tryjoin(U_pthread_t thread, void **thread_return)
{
  U_pthread_internal_t *internal_thread = (U_pthread_internal_t *) thread;

  dbg("pthread_tryjoin(%p)\n", thread);

  /* Verify that thread refers to a valid pthread */

  if (!U_pthread_valid(internal_thread))
    {
      return ESRCH;
    }

  /* First, check if the thread is defunct and waiting in the defunct list to
   * be detached. */

  if (internal_thread->state == PTHREAD_STATE_DEFUNCT)
    {
      if (thread_return)
        {
          /* Return the saved thread return value */

          *thread_return = internal_thread->retval;
        }

      /* Now we can remove the thread from the defunct list */

      U_pthread_queue_remove(&defunct, internal_thread);

      /* and release the thread structure */

      U_pthread_deallocate(internal_thread);
      dbg("pthread_tryjoin(): joined thread=%p\n", thread);
      return 0;
    }

  /* Check if the thread has been detached */

  if (internal_thread->flags & PTHREAD_FLAG_DETACHED)
    {
      /* Can't join to a detached thread */

      return EINVAL;
    }

  /* Otherwise, the thread is still running and has not been detached. */

  return EBUSY;
}

/**************************************************************************
 * Public Functions
 **************************************************************************/

int U_pthread_join(U_pthread_t thread, void **thread_return)
{
  int retval;

  dbg("pthread_join(%p)\n", thread);

  /* Loop until the join is performed */

  do
    {
      retval = U_pthread_tryjoin(thread, thread_return);
      if (retval == EBUSY)
        {
          /* EBUSY means that the thread is still attached and running
           * Otherwise, wait for the thread to exit */

          /* Mark the thread waiting for the join */

          current->state = PTHREAD_STATE_JOIN_WAITING;
          current->join = (U_pthread_internal_t *) thread;

          /* Remove the current thread from the ready-to-run lists */

          U_pthread_unready(current);

          /* Add it to a blocked list and return to the scheduler */

          U_pthread_block(&join_waiting, current);
          U_pthread_schedule();

          /* When we get here, the join should be complete in one way or
           * another. */
        }
    }
  while (retval == EBUSY);

  return retval;
}
