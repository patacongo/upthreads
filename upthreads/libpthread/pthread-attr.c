
/**************************************************************************
 * pthread-condattr.c
 * Miscellaneous stubs for pthread_condattr_* functions
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "pthread-config.h"

/* sched.h must be included first because is provides some otherwise lethal
 * defines.
 */

#ifdef CONFIG_HAVE_SCHED_H
#  include <sched.h>            /* struct sched_param */
#endif

#include "pthread-internal.h"   /* PTHREAD_PRIORITY_DEFAULT */

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

/**************************************************************************
 * Public Functions
 **************************************************************************/

/* Create / destruction */

int U_pthread_attr_init(U_pthread_attr_t * attr)
{
  if (!attr)
    {
      return EINVAL;
    }

  attr->sched_priority = PTHREAD_PRIORITY_DEFAULT;
  attr->detachstate = 0;
  attr->stacksize = CONFIG_DEFAULT_STACKSIZE;
  return 0;
}

int U_pthread_attr_destroy(U_pthread_attr_t * attr)
{
  return 0;
}

/* Stack size */

#ifdef CONFIG_MEMORY_ALLOCATOR
int U_pthread_attr_setstacksize(U_pthread_attr_t * attr, size_t stacksize)
{
  if (attr && stacksize > CONFIG_MIN_STACKSIZE)
    {
      attr->stacksize = stacksize;
      return 0;
    }

  return EINVAL;
}
#else
int U_pthread_attr_setstacksize(U_pthread_attr_t * attr, size_t stacksize)
{
  return EINVAL;
}
#endif

int
U_pthread_attr_getstacksize(const U_pthread_attr_t * attr, size_t * stacksize)
{
  if (attr && stacksize)
    {
      *stacksize = attr->stacksize;
      return 0;
    }

  return EINVAL;
}

/* Execution priority */

int
pthread_attr_setschedparam(U_pthread_attr_t * attr,
                           const struct sched_param *param)
{
  if (attr && param)
    {
      attr->sched_priority = param->sched_priority;
      return 0;
    }

  return EINVAL;
}

int
U_pthread_attr_getschedparam(const U_pthread_attr_t * attr,
                             struct sched_param *param)
{
  if (attr && param)
    {
      param->sched_priority = attr->sched_priority;
      return 0;
    }

  return EINVAL;
}

/* Detached state */

int U_pthread_attr_setdetachstate(U_pthread_attr_t * attr, int detachstate)
{
  if (attr)
    {
      attr->detachstate = detachstate;
      return 0;
    }

  return EINVAL;
}

int
U_pthread_attr_getdetachstate(const U_pthread_attr_t * attr, int *detachstate)
{
  if (attr && detachstate)
    {
      *detachstate = attr->detachstate;
      return 0;
    }

  return EINVAL;
}
