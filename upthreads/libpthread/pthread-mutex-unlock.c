
/**************************************************************************
 * pthread-mutex-unlock.c
 * Implements pthread_mutex_unlock()
 *
 *   Copyright (C) 2006 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <spudmonkey@racsa.co.cr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name Gregory Nutt nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************/

/**************************************************************************
 * Conditional Compilation Options
 **************************************************************************/

/**************************************************************************
 * Included Files
 **************************************************************************/

#include "pthread-internal.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

/**************************************************************************
 * Private Types
 **************************************************************************/

/**************************************************************************
 * Private Function Prototypes
 **************************************************************************/

/**************************************************************************
 * Global Variables
 **************************************************************************/

/**************************************************************************
 * Private Variables
 **************************************************************************/

/**************************************************************************
 * Private Functions
 **************************************************************************/

/**************************************************************************
 * Public Functions
 **************************************************************************/

int pthread_mutex_unlock(pthread_mutex_t * mutex)
{
  int i;

  dbg("pthread_mutex_unlock(%p)\n", mutex);

  /* Note that since we are, in reality, single threaded this really pretty
   * simple stuff. */

  if (!mutex)
    {
      return EINVAL;
    }

  /* Check for the case where the caller does not hold the mutex.  This could
   * mean that the mutex is not locked (mutex->locked < 1) or that the holder
   * is not the current thread. */

  if (mutex->locked <= 0 || mutex->holder != current)
    {
      dbg
        ("pthread_mutex_unlock: caller does not hold the mutex current=%p holder=%p\n",
         current, mutex->holder);
      return EPERM;
    }

  /* Release the mutex */

  /* Unlock the mutex */

  dbg3("pthread_mutex_unlock: current=%p releases mutex\n", current);

  /* The mutex is recursive */

  if (--mutex->locked > 0)
    {
      return 0;
    }

  /* No longer held by this user */

  mutex->holder = NULL;

  /* Find the highest priority thread waiting for this mutex (if any) */

  for (i = 0; i < mutex_waiting.length; i++)
    {
      U_pthread_internal_t *waiter = mutex_waiting.list[i];

      /* Is this thread waiting for this mutex? */

      if (waiter->state == PTHREAD_STATE_MUTEX_WAITING
          && waiter->mutex == mutex)
        {
          /* Yes, give the mutex to the waiter */

          dbg2("pthread_mutex_unlock: Giving thread to waiter %p\n", waiter);
          mutex->locked = 1;
          mutex->holder = waiter;

          /* Remove the waiting thread from the blocked list */

          U_pthread_unblock(&mutex_waiting, waiter);

          /* Then put the thread into the ready-to-run list. Note that we do
           * not give control to the unblocked waiter until the current thread
           * returns. */

          waiter->state = PTHREAD_STATE_READYTORUN;
          U_pthread_readytorun(waiter);

          /* Check if we just started a higher priority thread then ourself and 
           * if the scheduler is unlocked. */

          if (waiter->priority < current->priority
              && (current->flags & PTHREAD_FLAG_SCHEDLOCK) == 0)
            {
              /* Then let it run */

              U_pthread_schedule();
            }

          /* And break out of the loop -- only one thread can have the mutex */

          break;
        }
    }

  /* Return success */

  return 0;
}
